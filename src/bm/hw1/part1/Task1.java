package bm.hw1.part1;

/*
Объем шара
Вычислите и выведите на экран объем шара, получив его радиус r с консоли.

Подсказка:
Считать по формуле V  =  4/3  pi  r^3. Значение числа pi взять из Math.

Ограничения:
 0 < r < 100
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        System.out.println(4.0 / 3.0 * Math.PI * Math.pow(new Scanner(System.in).nextDouble(), 3));
    }
}
