package bm.hw1.part1;

/*
Привет
Прочитайте из консоли имя пользователя и выведите в консоль строку:
Привет, <имя пользователя>!

Подсказка:
Получите данные из консоли c помощью объекта Scanner, сохраните в переменную userName и выведите в консоль с помощью System.out.println()
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        System.out.printf("Привет, %s!", new Scanner(System.in).nextLine());
    }
}
