package bm.hw1.part1;

/*
Километры в мили
На вход подается количество километров count.
Переведите километры в мили (1 миля = 1,60934 км) и выведите количество миль.

Ограничения:
0 < count < 1000
 */

import java.util.Scanner;

public class Task6 {

    static final double KM = 1.60934;

    public static void main(String[] args) {
        System.out.println(new Scanner(System.in).nextInt() / KM);
    }
}
