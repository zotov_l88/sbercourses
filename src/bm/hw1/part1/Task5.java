package bm.hw1.part1;

/*
Дюймы в сантиметры
Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров).
На вход подается количество дюймов count, выведите количество сантиметров.

Ограничения:
0 < count < 1000
 */

import java.util.Scanner;

public class Task5 {

    static final double INCH = 2.54;

    public static void main(String[] args) {
        System.out.println(INCH * new Scanner(System.in).nextInt());
    }
}
