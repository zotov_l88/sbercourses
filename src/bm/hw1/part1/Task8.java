package bm.hw1.part1;

/*
Бюджет
На вход подается баланс счета в банке – n.
Рассчитайте дневной бюджет на 30 дней.

Ограничение:
0 < count < 100000
 */

import java.util.Scanner;

public class Task8 {

    static final int MONTH = 30;

    public static void main(String[] args) {
        System.out.println(new Scanner(System.in).nextDouble() / MONTH);
    }
}
