package bm.hw1.part1;
/*
Перестановка цифр
На вход подается двузначное число n.
Выведите число, полученное перестановкой цифр в исходном числе n.
Если после перестановки получается ведущий 0, его также надо вывести.
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        System.out.printf("%d%d", n % 10, n / 10);
    }
}
