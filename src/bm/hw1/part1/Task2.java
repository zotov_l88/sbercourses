package bm.hw1.part1;

/*
Среднее квадратичное
На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.

Подсказка:
Среднее квадратическое: https://en.wikipedia.org/wiki/Rootmeansquare
Для вычисления квадратного корня воспользуйтесь функцией Math.sqrt(x)
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println(Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2));
    }
}
