package bm.hw1.part1;

/*
Текущее время
На вход подается количество секунд, прошедших с начала текущего дня – count.
Выведите в консоль текущее время в формате: часы и минуты.

Ограничения:
0 < count < 86400
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        int seconds = new Scanner(System.in).nextInt();
        int hours = seconds / 3600;
        int minutes = (seconds - hours * 3600) / 60;
        System.out.printf("%d %d", hours, minutes);
    }
}
