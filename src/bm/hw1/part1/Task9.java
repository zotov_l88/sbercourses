package bm.hw1.part1;

/*
Количество гостей
На вход подается бюджет мероприятия – n тугриков.
Бюджет на одного гостя – k тугриков.
Вычислите и выведите, сколько гостей можно пригласить на мероприятие.

Ограничение:
0 < n < 100000
0 < k < 1000
k < n
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println(a / b);
    }
}
