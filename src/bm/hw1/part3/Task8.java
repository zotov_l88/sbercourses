package bm.hw1.part3;

/*
Вычислить сумму
На вход подается:
- целое число n
- целое число p
- целые числа a1, a2 , … an

Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго больше p.

Ограничение:
0 < m, n, ai < 1000
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int p = sc.nextInt();
        int sum = 0;
        int num;
        while (n-- > 0) {
            num = sc.nextInt();
            if (num > p) {
                sum += num;
            }
        }
        System.out.println(sum);
    }
}
