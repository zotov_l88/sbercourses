package bm.hw1.part3;

/*
Елочка
Вывести на экран "ёлочку" из символа решетки (#) заданной высоты N.
На N + 1 строке у "ёлочки" должен быть отображен ствол из символа |

Ограничение:
2 < n < 10
 */

import java.util.Scanner;

public class Task10 {

    public static void main(String[] args) {
        int n = new Scanner(System.in).nextInt();
        int end = n * 2 - 1;
        int line = n;
        int currentLine = line;
        int sym = 1;
        int currentSym = sym;
        int rootLine = line;
        int rootSym = sym;

        while (n <= end) {
            while (currentLine > 0) {
                if (currentLine == currentSym) {
                    while (currentSym-- > 0) {
                        System.out.print("#");
                        currentLine--;
                    }
                } else {
                    System.out.print(" ");
                    currentLine--;
                }
            }
            line++;
            currentLine = line;
            sym += 2;
            currentSym = sym;
            n++;
            System.out.println();
        }
        while (rootLine > 0) {
            System.out.print(rootLine-- == rootSym ? "|" : " ");
        }
    }
}
