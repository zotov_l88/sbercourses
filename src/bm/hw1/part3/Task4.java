package bm.hw1.part3;

/*
Цифры в столбик
Дано натуральное число n. Вывести его цифры в "столбик".

Ограничение:
0 < n < 1000000
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        String n = new Scanner(System.in).nextLine();
        int length = n.length();
        int index = 0;
        while (index < length) {
            System.out.println(n.substring(0, 1));
            n = n.substring(1);
            index++;
        }
    }
}
