package bm.hw1.part3;

/*
Количество символов
Дана строка s.
Вычислить количество символов в ней, не считая пробелов (необходимо использовать цикл).

Ограничение:
0 < s.length() < 1000
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        String word = new Scanner(System.in).nextLine();
        int n = word.length() - 1;
        int count = 0;
        while (n >= 0) {
            if (word.charAt(n--) != ' ') {
                count++;
            }
        }
        System.out.println(count);
    }
}
