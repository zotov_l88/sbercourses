package bm.hw1.part3;

/*
Сумма чисел
На вход подается два положительных числа m и n.
Найти сумму чисел между m и n включительно.

Ограничение:
0 < m, n < 10
m < n
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int sum = 0;
        while (n <= m) {
            sum += n++;
        }
        System.out.println(sum);
    }
}
