package bm.hw1.part3;

/*
Вычислить выражение
На вход подается два положительных числа m и n.
Необходимо вычислить m^1 + m^2 + ... + m^n

Ограничение:
0 < m, n < 10
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int sum = 0;
        while (n > 0) {
            sum += Math.pow(m, n--);
        }
        System.out.println(sum);
    }
}
