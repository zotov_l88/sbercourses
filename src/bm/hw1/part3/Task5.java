package bm.hw1.part3;

/*
Взятие остатка
Даны положительные натуральные числа m и n.
Найти остаток от деления m на n, не выполняя операцию взятия остатка.

Ограничение:
0 < m, n < 10
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        System.out.println(m - m / n * n);
    }
}
