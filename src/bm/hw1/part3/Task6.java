package bm.hw1.part3;

/*
Количество купюр
В банкомате остались только купюры номиналом 8 4 2 1.
Дано положительное число n - количество денег для размена.
Необходимо найти минимальное количество купюр с помощью которых можно разменять это количество денег (соблюсти порядок: первым числом вывести количество купюр номиналом 8, вторым - 4 и т д).

Ограничение:
0 < n < 1000000
 */

import java.util.Scanner;

public class Task6 {

    static int eight = 0;
    static int four = 0;
    static int two = 0;
    static int one = 0;
    static int sum = new Scanner(System.in).nextInt();

    public static void main(String[] args) {
        eight = countBill(eight, 8);
        four = countBill(four, 4);
        two = countBill(two, 2);
        one = countBill(one, 1);
        System.out.printf("%s %s %s %s", eight, four, two, one);
    }

    private static int countBill(int bill, int denomination) {
        while (sum > 0) {
            sum -= denomination;
            bill++;
        }
        if (sum != 0) {
            sum += denomination;
            bill--;
        }
        return bill;
    }
}
