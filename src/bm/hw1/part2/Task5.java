package bm.hw1.part2;

/*
Квадратичное уравнение
Дома дочери Пети опять нужна помощь с математикой!
В этот раз ей нужно проверить, имеет ли предложенное квадратное уравнение решение или нет.

На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.

Ограничение:
-100 < a, b, c < 100
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isNoSolutions(sc.nextInt(), sc.nextInt(), sc.nextInt()) ? "Решения нет" : "Решение есть");
    }

    private static boolean isNoSolutions(int a, int b, int c) {
        return (b * b - 4 * a * c) < 0;
    }
}
