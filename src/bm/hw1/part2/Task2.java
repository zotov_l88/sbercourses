package bm.hw1.part2;

/*
Первый квадрант
Петя пришел домой и помогает дочке решать математику.
Ей нужно определить, принадлежит ли точка с указанными координатами первому квадранту.
Недолго думая, Петя решил автоматизировать процесс и написать программу: на вход нужно принимать два целых числа (координаты точки), выводить true, когда точка попала в квадрант и false иначе.
Но сначала Петя вспомнил, что точка лежит в первом квадранте тогда, когда её координаты удовлетворяют условию: x > 0 и y > 0.

Ограничение:
-100 < x, y < 100
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isOk(sc.nextInt(), sc.nextInt()));
    }

    private static boolean isOk(int a, int b) {
        return a > 0 && b > 0;
    }
}
