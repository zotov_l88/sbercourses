package bm.hw1.part2;

/*
Оценка на ревью
За каждый год работы Петя получает на ревью оценку.
На вход подаются оценки Пети за последние три года (три целых положительных числа).
Если последовательность оценок строго монотонно убывает, то вывести "Петя, пора трудиться"
В остальных случаях вывести "Петя молодец!"

Ограничение:
0 < a, b, c < 100
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isDown(sc.nextInt(), sc.nextInt(), sc.nextInt()) ? "Петя, пора трудиться" : "Петя молодец!");
    }

    private static boolean isDown(int a, int b, int c) {
        return a > b && b > c;
    }
}
