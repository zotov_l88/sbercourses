package bm.hw1.part2.additionally;

/*
Проверка посылки
У нас есть почтовая посылка (String mailPackage).
Каждая почтовая посылка проходит через руки проверяющего.
Работа проверяющего заключается в следующем:
 во-первых, посмотреть не пустая ли посылка;
 во-вторых, проверить нет ли в ней камней или запрещенной продукции.

Наличие камней или запрещенной продукции указывается в самой посылке в конце или в начале.
Если в посылке есть камни, то будет написано слово "камни!", если запрещенная продукция, то будет фраза "запрещенная продукция".
После осмотра посылки проверяющий должен сказать:
 "камни в посылке" – если в посылке есть камни;
 "в посылке запрещенная продукция" – если в посылке есть что-то запрещенное;
 "в посылке камни и запрещенная продукция" – если в посылке находятся камни и запрещенная продукция;
 "все ок" – если с посылкой все хорошо.

Если посылка пустая, то с посылкой все хорошо.
Напишите программу, которая будет заменять проверяющего.
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        checkPost(new Scanner(System.in).nextLine());
    }

    private static void checkPost(String post) {
        int rocks, forbidden;
        if (!post.isEmpty()) {
            rocks = post.contains("камни") ? 1 : 0;
            forbidden = post.contains("запрещенная продукция") ? 1 : 0;
            verdict(rocks, forbidden);
        } else {
            System.out.println("все ок");
        }
    }

    private static void verdict(int rocks, int forbidden) {
        if (rocks == 1 && forbidden == 1) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (rocks == 1) {
            System.out.println("камни в посылке");
        } else if (forbidden == 1) {
            System.out.println("в посылке запрещенная продукция");
        } else {
            System.out.println("все ок");
        }
    }
}
