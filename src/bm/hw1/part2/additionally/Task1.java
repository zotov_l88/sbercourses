package bm.hw1.part2.additionally;

/*
Проверить пароль
У Марата был взломан пароль.
Он решил написать программу, которая проверяет его пароль на сложность.

В интернете он узнал, что пароль должен отвечать следующим требованиям:
 пароль должен состоять из хотя бы 8 символов
В пароле должны быть:
 заглавные буквы
 строчные символы
 числа
 специальные знаки (_-)

Если пароль прошел проверку, то программа должна вывести в консоль строку пароль надежный, иначе строку: пароль не прошел проверку

 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        System.out.println(isPasswordStrong(new Scanner(System.in).nextLine()) ?
                "пароль надежный" : "пароль не прошел проверку");
    }

    private static boolean isPasswordStrong(String password) {
        return isCountSymbolsBiggerThanSeven(password) &&
                isPasswordContainsUpperCase(password) &&
                isPasswordContainsLowerCase(password) &&
                isPasswordContainsDigit(password) &&
                isPasswordContainsSpecialSymbols(password);
    }

    private static boolean isCountSymbolsBiggerThanSeven(String password) {
        return password.length() > 7;
    }

    private static boolean isPasswordContainsUpperCase(String password) {
        boolean result = false;
        int count = password.length() - 1;
        while (count >= 0) {
            if (password.charAt(count) >= 65 && password.charAt(count) <= 90) {
                result = true;
                break;
            }
            count--;
        }
        return result;
    }

    private static boolean isPasswordContainsLowerCase(String password) {
        boolean result = false;
        int count = password.length() - 1;
        while (count >= 0) {
            if (password.charAt(count) >= 97 && password.charAt(count) <= 122) {
                result = true;
                break;
            }
            count--;
        }
        return result;
    }

    private static boolean isPasswordContainsDigit(String password) {
        return password.contains("-") || password.contains("_");
    }

    private static boolean isPasswordContainsSpecialSymbols(String password) {
        boolean result = false;
        int count = password.length() - 1;
        while (count >= 0) {
            if (Character.isDigit(password.charAt(count))) {
                result = true;
                break;
            }
            count--;
        }
        return result;
    }
}
