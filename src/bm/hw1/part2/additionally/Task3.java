package bm.hw1.part2.additionally;

/*
Покупка телефона
Старый телефон Андрея сломался, поэтому он решил приобрести новый.
Продавец телефонов предлагает разные варианты, но Андрея интересуют только модели серии samsung или iphone.
Также Андрей решил рассматривать телефоны только от 50000 до 120000 рублей.
Чтобы не тратить время на разговоры, Андрей хочет написать программу, которая поможет ему сделать выбор.

На вход подается строка – модель телефона и число – стоимость телефона.
Нужно вывести "Можно купить", если модель содержит слово samsung или iphone и стоимость от 50000 до 120000 рублей включительно.
Иначе вывести "Не подходит".

Гарантируется, что в модели телефона не указано одновременно несколько серий.
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isOk(sc.nextLine(), sc.nextInt()) ? "Можно купить" : "Не подходит");
    }

    private static boolean isOk(String model, int price) {
        return (model.contains("samsung") || model.contains("iphone"))
                && price >= 50_000 && price <= 120_000;
    }
}
