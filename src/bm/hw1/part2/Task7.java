package bm.hw1.part2;

/*
Разделение строки 1
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу.
Для этого он может воспользоваться методами indexOf() и substring().

На вход подается строка. Нужно вывести две строки, полученные из входной разделением по первому пробелу.

Ограничение:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        String str = new Scanner(System.in).nextLine();
        System.out.println(str.substring(0, str.indexOf(" ")));
        System.out.println(str.substring(str.indexOf(" ") + 1));
    }
}

