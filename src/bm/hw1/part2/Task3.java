package bm.hw1.part2;

/*
Время обедать
Петя снова пошел на работу.
С сегодняшнего дня он решил ходить на обед строго после полудня.
Периодически он посматривает на часы (x - час, который он увидел).
Помогите Пете решить, пора ли ему на обед или нет.
Если время больше полудня, то вывести "Пора". Иначе - “Рано”.

Ограничение:
0 <= n <= 23
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isOk(sc.nextInt()) ? "Пора" : "Рано");
    }

    private static boolean isOk(int a) {
        return a > 12;
    }
}
