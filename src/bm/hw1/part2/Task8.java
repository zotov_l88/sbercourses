package bm.hw1.part2;

/*
Разделение строки 2
Раз так легко получается разделять по первому пробелу, Петя решил немного изменить предыдущую программу и теперь разделять строку по последнему пробелу.

Ограничение:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        String str = new Scanner(System.in).nextLine();
        System.out.println(str.substring(0, str.lastIndexOf(" ")));
        System.out.println(str.substring(str.lastIndexOf(" ") + 1));
    }
}

