package bm.hw1.part2;

/*
Составить треугольник
Разобравшись со своими (и не только) задачками, Петя уже собирался лечь спать и отдохнуть перед очередным тяжелым рабочим днем, но вдруг в тишине раздается детский шепот:
"Паааапааа, мы забыли решить ещё одну задачку! Давай проверим, можно ли из трех сторон составить треугольник?".
Что ж, придется написать еще одну программу, связанную со школьной математикой.

На вход подается три целых положительных числа – длины сторон треугольника.
Нужно вывести true, если можно составить треугольник из этих сторон и false иначе.

Ограничение:
0 < a, b, c < 100
 */

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isTriangleExist(sc.nextInt(), sc.nextInt(), sc.nextInt()));
    }

    private static boolean isTriangleExist(int a, int b, int c) {
        return a + b > c && a + c > b && b + c > a;
    }
}

