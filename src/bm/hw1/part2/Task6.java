package bm.hw1.part2;

/*
Уровень английского
На следующий день на работе Петю и его коллег попросили заполнить анкету.
Один из вопросов был про уровень владения английского.
Петя и его коллеги примерно представляют, сколько они знают иностранных слов.
Также у них есть табличка перевода количества слов в уровень владения английском языком.
Было бы здорово автоматизировать этот перевод!

На вход подается положительное целое число count - количество выученных иностранных слов.
Нужно вывести какому уровню соответствует это количество.

| Количество слов    | Уровень английского |
|----------------------|-----------------------|
| count < 500          | beginner              |
| 500 <= count < 1500  | pre-intermediate      |
| 1500 <= count < 2500 | intermediate          |
| 2500 <= count < 3500 | upper-intermediate    |
| 3500 <= count        | fluent                |

Ограничение:
0 <= count < 10000
 */

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        lvlEnglish(sc.nextInt());
    }

    private static void lvlEnglish(int a) {
        if (a < 500) {
            System.out.println("beginner");
        } else if (a < 1500) {
            System.out.println("pre-intermediate");
        } else if (a < 2500) {
            System.out.println("intermediate");
        } else if (a < 3500) {
            System.out.println("upper-intermediate");
        } else {
            System.out.println("fluent");
        }
    }
}
