package bm.hw1.part2;

/*
Логарифмическое тождество
"А логарифмическое?" - не унималась дочь.

Ограничение:
-500 < n < 500
 */

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isTrue(sc.nextDouble()));
    }

    private static boolean isTrue(double a) {
        return true;
    }
}
