package bm.hw3.part1.task7;

public class TriangleChecker {

    public static boolean isTriangleExist(final double a, final double b, final double c) {
        return a + b > c && a + c > b && c + b > a;
    }
}
