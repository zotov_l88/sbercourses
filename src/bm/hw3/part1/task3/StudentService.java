package bm.hw3.part1.task3;

import bm.hw3.part1.task2.Student;

import java.util.Random;

public class StudentService {

    public Student bestStudent(Student[] students) {
        Student bestStudent = null;
        double avgGrade = 0;
        for (Student student : students) {
            double tmp = student.avgGrade();
            if (tmp > avgGrade) {
                avgGrade = tmp;
                bestStudent = student;
            }
        }
        return bestStudent;
    }

    public void sortBySurname(Student[] students) {
        for (int i = 0; i < students.length - 1; i++) {
            boolean isSort = true;
            for (int j = 0; j < students.length - 1 - i; j++) {
                if ((students[j + 1].compareTo(students[j])) < 0) {
                    swap(students, j);
                    isSort = false;
                }
            }
            if (isSort) {
                break;
            }
        }
    }

    private void swap(Student[] students, int i) {
        Student tmp = students[i + 1];
        students[i + 1] = students[i];
        students[i] = tmp;
    }

    public void setGrades(Student student) {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            student.addGrade(random.nextInt(5) + 1);
        }
    }
}
