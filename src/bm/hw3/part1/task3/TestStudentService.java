package bm.hw3.part1.task3;

import bm.hw3.part1.task2.Student;

public class TestStudentService {
    public static void main(String[] args) {
        StudentService studentService = new StudentService();
        Student one = new Student("Peter", "Parker");
        Student two = new Student("Simple", "Man");
        Student three = new Student("Ivan", "Bivanov");
        Student four = new Student("Alex", "Alexov");
        Student five = new Student("Misha", "Alexovich");
        Student[] students = {one, two, three, four, five};

        System.out.println("\n***Grades***");
        for (Student student : students) {
            studentService.setGrades(student);
            System.out.println(student);
        }

        System.out.println("\n***After sortBySurname***");
        studentService.sortBySurname(students);
        for (Student student : students) {
            System.out.println(student);
        }

        System.out.println("\n***All avg grades***");
        for (Student student : students) {
            System.out.println(student + " have grade: " + student.avgGrade());
        }
        System.out.println("\n***Best student***");
        System.out.println(studentService.bestStudent(students));
    }
}
