package bm.hw3.part1.task6;

public class TestAmazingString {
    public static void main(String[] args) {
        AmazingString one = new AmazingString(new char[]{'H', 'e', 'l', 'l', 'o'});
        AmazingString two = new AmazingString("Hello");
        String three = "World";
        AmazingString four = new AmazingString("     _#434World  s  DFwdf  ");
        AmazingString five = new AmazingString("      12345    ");
        AmazingString six = two.inverse();
        AmazingString seven = five.trim();

        System.out.println(one.getChar(2));
        System.out.println(two.length());
        six.printString();
        seven.printString();
        System.out.println(seven.length());
        System.out.println(four.contains(three));
        System.out.println(new AmazingString("123abC").contains(new char[]{'1', '2', '3', 'a', 'b', 'C'}));
        System.out.println(new AmazingString(" ").contains(new char[]{' '}));
        System.out.println(new AmazingString("").contains(new char[]{'1'}));
        System.out.println(new AmazingString("").contains(new char[]{}));
        System.out.println(new AmazingString("String").contains(""));
    }
}
