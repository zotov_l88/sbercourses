package bm.hw3.part1.task6;

// объект класса по аналогии с классом String не изменяемый
public final class AmazingString {

    private static final char SPACE = ' ';
    private final char[] string;

    public AmazingString(char[] string) {
        this.string = string;
    }

    public AmazingString(String string) {
        this.string = convertStringToChars(string);
    }

    private char[] convertStringToChars(String string) {
        StringBuilder sb = new StringBuilder(string);
        char[] arr = new char[sb.length()];
        sb.getChars(0, sb.length(), arr, 0);
        return arr;
    }

    // возвращает i char массива или исключение
    public char getChar(int index) {
        if (index > -1 && index < string.length) {
            return string[index];
        } else {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    // возвращает длину массива
    public int length() {
        return string.length;
    }

    // выводит на экран содержимое массива
    public void printString() {
        for (char c : string) {
            System.out.print(c);
        }
        System.out.println();
    }

    // разворачивает массив и присваивает полученное значение новому объекту
    public AmazingString inverse() {
        AmazingString result = new AmazingString(new char[this.length()]);
        if (length() > 0) {
            for (int i = string.length - 1, j = 0; i >= 0; i--, j++) {
                result.string[j] = this.string[i];
            }
        }
        return result;
    }

    /*
    Обрезает начальные и конечные пробелы в массиве. Для этого определяем количество пробелов в начале
    и в конце массива, затем создаем новый объект длиной "длина старого массива - количество пробелов"
    и наполняем новый массив созданного объекта
     */
    public AmazingString trim() {
        AmazingString result;
        int start = spacesOfStart(string);
        int end = spacesOfStart(inverse().string);
        if (length() - start - end < 0) {
            result = new AmazingString("");
        } else {
            result = new AmazingString(new char[this.length() - start - end]);
            for (int i = start, j = 0; i < string.length - end; i++, j++) {
                result.string[j] = this.string[i];
            }
        }
        return result;
    }

    // возвращает количество пробелов до первого иного символа
    private int spacesOfStart(char[] string) {
        int count = 0;
        for (char c : string) {
            if (c != SPACE) {
                break;
            }
            count++;
        }
        return count;
    }

    // возвращает true, если src содержится в string
    public boolean contains(char[] src) {
        boolean isContains = false;
        if (string.length >= src.length && src.length > 0) {
            int lenString = string.length;
            int start = 0;
            while (lenString-- >= src.length) {
                if (string[start] == src[0]) {
                    if (startOf(string, src, start)) {
                        isContains = true;
                        break;
                    }
                }
                start++;
            }
        }
        return src.length == 0 || isContains;
    }

    // аналог предыдущей функции, но в качестве аргумента объект String
    public boolean contains(String src) {
        return contains(src.toCharArray());
    }

    // возвращает true, если dst с указанного индекса start совпадает с содержимым src
    private boolean startOf(char[] dst, char[] src, int start) {
        boolean isEquals = true;
        for (int i = 0, j = start; i < src.length; i++, j++) {
            if (src[i] != dst[j]) {
                isEquals = false;
                break;
            }
        }
        return isEquals;
    }
}
