package bm.hw3.part1.task2;

public class TestStudent {
    public static void main(String[] args) {
        Student student = new Student("John", "Connor");
        for (int i = 0; i < 15; i++) {
            student.addGrade(i);
        }
        System.out.println(student);
    }
}
