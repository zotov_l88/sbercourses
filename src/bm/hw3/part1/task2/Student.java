package bm.hw3.part1.task2;

import java.util.Arrays;

public class Student implements Comparable<Student> {

    private String name;
    private String surname;
    private int[] grades;
    private final static int SIZE = 10;

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.grades = new int[SIZE];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        shift();
        grades[SIZE - 1] = grade;
    }

    private void shift() {
        for (int i = 0; i < SIZE - 1; i++) {
            grades[i] = grades[i + 1];
        }
    }

    public double avgGrade() {
        double sumOfGrades = 0;
        int countOfGrades = 0;
        for (int i = 0; i < this.getGrades().length; i++) {
            if (this.getGrades()[i] > 0) {
                sumOfGrades += this.getGrades()[i];
                countOfGrades++;
            }
        }
        return sumOfGrades / countOfGrades;
    }

    @Override
    public int compareTo(Student student) {
        return this.getSurname().compareTo(student.getSurname());
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
