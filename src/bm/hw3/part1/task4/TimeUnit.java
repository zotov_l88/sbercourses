package bm.hw3.part1.task4;

import bm.hw3.part1.task4.Exceptions.TimeUnitHoursException;
import bm.hw3.part1.task4.Exceptions.TimeUnitMinutesException;
import bm.hw3.part1.task4.Exceptions.TimeUnitSecondsException;

public class TimeUnit {

    private int hours;
    private int minutes;
    private int seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        try {
            if (hours >= 0 && hours <= 23) {
                this.hours = hours;
            } else {
                throw new TimeUnitHoursException();
            }
        } catch (TimeUnitHoursException e) {
            e.errorMessage(hours);
            System.exit(1);
        }
        try {
            if (minutes >= 0 && minutes <= 59) {
                this.minutes = minutes;
            } else {
                throw new TimeUnitMinutesException();
            }
        } catch (TimeUnitMinutesException e) {
            e.errorMessage(minutes);
            System.exit(2);
        }
        try {
            if (seconds >= 0 && seconds <= 59) {
                this.seconds = seconds;
            } else {
                throw new TimeUnitSecondsException();
            }
        } catch (TimeUnitSecondsException e) {
            e.errorMessage(seconds);
            System.exit(3);
        }
    }

    public TimeUnit(int hours, int minutes) {
        this(hours, minutes, 0);
    }

    public TimeUnit(int hours) {
        this(hours, 0, 0);
    }

    public void showTimeFormat24() {
        System.out.printf("%02d:%02d:%02d\n", hours, minutes, seconds);
    }

    public void showTimeFormat12() {
        String format = (hours > 11) ? "pm" : "am";
        int h = (hours > 12) ? hours % 12 : hours;
        h = (hours == 0) ? 12 : h;
        System.out.printf("%02d:%02d:%02d %s\n", h, minutes, seconds, format);
    }

    public void add(final int hours, final int minutes, final int seconds) {
        this.hours += hours;
        this.minutes += minutes;
        this.seconds += seconds;
        correctTime();
    }

    private void correctTime() {
        int add = seconds / 60;
        seconds = seconds % 60;
        minutes += add;
        add = minutes / 60;
        minutes = minutes % 60;
        hours += add;
        hours = hours > 23 ? hours % 24 : hours;
    }
}
