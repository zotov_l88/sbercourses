package bm.hw3.part1.task4.Exceptions;

public class TimeUnitHoursException extends TimeUnitException {

    @Override
    public void errorMessage(int val) {
        super.errorMessage(val);
        System.out.println("For hours use numbers in range 0 - 23.");
    }
}
