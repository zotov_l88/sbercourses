package bm.hw3.part1.task4.Exceptions;

public class TimeUnitSecondsException extends TimeUnitException {

    @Override
    public void errorMessage(int val) {
        super.errorMessage(val);
        System.out.println("For seconds use numbers in range 0 - 59.");
    }
}
