package bm.hw3.part1.task4.Exceptions;

public class TimeUnitException extends Exception {

    public void errorMessage(int val) {
        System.out.printf("%s %d.\n", "Illegal argument", val);
    }
}
