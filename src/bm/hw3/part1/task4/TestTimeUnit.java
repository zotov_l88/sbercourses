package bm.hw3.part1.task4;

public class TestTimeUnit {
    public static void main(String[] args) {
        System.out.println("**********ONE**********");
        TimeUnit one = new TimeUnit(23, 59, 59);
        one.showTimeFormat24();
        one.showTimeFormat12();
        one.add(0, 0, 1);
        one.showTimeFormat24();
        one.showTimeFormat12();
        one.add(0, 0, 1);
        one.showTimeFormat24();
        one.showTimeFormat12();
        System.out.println("**********TWO**********");
        TimeUnit two = new TimeUnit(0,0,1);
        two.showTimeFormat24();
        two.showTimeFormat12();
        two.add(0, 0, 12345); // check this https://tools.icoder.uz/seconds-to-hms-time.php
        two.showTimeFormat24();
        two.showTimeFormat12();
        System.out.println("**********THREE**********");
        TimeUnit three = new TimeUnit(23,32);
        three.showTimeFormat24();
        three.showTimeFormat12();
        three.add(100, 1000, 10000);
        three.showTimeFormat24();
        three.showTimeFormat12();
        System.out.println("**********FOUR**********");
        TimeUnit four = new TimeUnit(1);
        four.showTimeFormat24();
        four.showTimeFormat12();
        four.add(11, 59, 12);
        four.showTimeFormat24();
        four.showTimeFormat12();
    }
}
