package bm.hw3.part1.task8;

public class Atm {

    private final double exchangeRateDollarToRubles;
    private final double exchangeRateRublesToDollar;
    private static int countOfCreateAtm;

    public Atm(double exchangeRateDollarToRubles, double exchangeRateRublesToDollar) {
        if (exchangeRateDollarToRubles > 0 && exchangeRateRublesToDollar > 0) {
            this.exchangeRateDollarToRubles = exchangeRateDollarToRubles;
            this.exchangeRateRublesToDollar = exchangeRateRublesToDollar;
            countOfCreateAtm++;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public double convertDollarToRubles(final double dollar) {
        return dollar * exchangeRateDollarToRubles;
    }

    public double convertRublesToDollar(final double rubles) {
        return rubles * exchangeRateRublesToDollar;
    }

    public static int getCountOfInstance() {
        return countOfCreateAtm;
    }
}
