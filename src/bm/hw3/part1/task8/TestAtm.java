package bm.hw3.part1.task8;

public class TestAtm {
    public static void main(String[] args) {
        Atm one = new Atm(70, 0.02);
        System.out.println(one.convertDollarToRubles(11));
        System.out.println(one.convertRublesToDollar(70));

        Atm two = new Atm(100.1, 0.0051);
        System.out.println(two.convertDollarToRubles(100));
        System.out.println(two.convertRublesToDollar(5000));

        System.out.println(Atm.getCountOfInstance());
    }
}
