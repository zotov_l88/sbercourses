package bm.hw3.part1.task5;

import bm.hw3.part1.task5.Exceptions.IllegalDayOfWeekException;
import bm.hw3.part1.task5.Exceptions.IllegalNumberOfDayOfWeekException;

public class TestDayOfWeek {
    public static void main(String[] args) {
        try {
            System.out.println(DayOfWeek.getDay((byte) 2));
        } catch (IllegalNumberOfDayOfWeekException e) {
            e.errorMessage();
        }

        try {
            System.out.println(DayOfWeek.getNumberOfDay("четверг"));
        } catch (IllegalDayOfWeekException e) {
            e.errorMessage();
        }

        DayOfWeek.allDaysOfWeek();
    }
}
