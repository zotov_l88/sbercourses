package bm.hw3.part1.task5.Exceptions;

public class IllegalDayOfWeekException extends DayOfWeekException {

    @Override
    public void errorMessage() {
        System.out.println("This day is not exist");
    }
}
