package bm.hw3.part1.task5.Exceptions;

public class IllegalNumberOfDayOfWeekException extends DayOfWeekException {

    @Override
    public void errorMessage() {
        System.out.println("The number of day must be from 1 to 7 includes");
    }
}
