package bm.hw3.part1.task5;

public enum Days {
    MONDAY((byte) 1, "Понедельник"),
    TUESDAY((byte) 2, "Вторник"),
    WEDNESDAY((byte) 3, "Среда"),
    THURSDAY((byte) 4, "Четверг"),
    FRIDAY((byte) 5, "Пятница"),
    SATURDAY((byte) 6, "Суббота"),
    SUNDAY((byte) 7, "Воскресенье");

    final byte numberOfDay;
    final String day;

    Days(byte numberOfDay, String day) {
        this.numberOfDay = numberOfDay;
        this.day = day;
    }
}
