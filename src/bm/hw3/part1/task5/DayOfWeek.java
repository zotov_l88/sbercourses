package bm.hw3.part1.task5;

import bm.hw3.part1.task5.Exceptions.IllegalDayOfWeekException;
import bm.hw3.part1.task5.Exceptions.IllegalNumberOfDayOfWeekException;

public class DayOfWeek {

    private Days DAYS;

    public static void allDaysOfWeek() {
        for (Days val : Days.values()) {
            System.out.printf("%d %s\n", val.numberOfDay, val.day);
        }
    }

    public static byte getNumberOfDay(String day) throws IllegalDayOfWeekException {
        byte result = 0;
        for (Days val : Days.values()) {
            if (val.day.equalsIgnoreCase(day)) {
                result = val.numberOfDay;
                break;
            }
        }
        if (result != 0) {
            return result;
        } else {
            throw new IllegalDayOfWeekException();
        }
    }

    public static String getDay(byte n) throws IllegalNumberOfDayOfWeekException {
        String result = null;
        for (Days val : Days.values()) {
            if (n == val.numberOfDay) {
                result = val.day;
            }
        }
        if (result != null) {
            return result;
        } else {
            throw new IllegalNumberOfDayOfWeekException();
        }
    }
}

