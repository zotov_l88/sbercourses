package bm.hw3.part3.task2;

public class Main {
    public static void main(String[] args) {

        BestCarpenterEver bce = new BestCarpenterEver();
        for (Furniture furniture : new Furniture[]{new Stool(), new Table()}) {
            System.out.println(bce.isFix(furniture));
        }

    }
}
