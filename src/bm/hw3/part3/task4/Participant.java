package bm.hw3.part3.task4;

import java.util.ArrayList;

public class Participant {

    private final Person person;
    private final Dog dog;
    private final ArrayList<Double> rates;

    public Participant(Person person, Dog dog, ArrayList<Double> rates) {
        this.person = person;
        this.dog = dog;
        this.rates = rates;
    }

    public Person getPerson() {
        return person;
    }

    public Dog getDog() {
        return dog;
    }

    public double avgRate() {
        double result = 0;
        for (Double rate : rates) {
            result += rate;
        }
        return result / rates.size();
    }
}
