package bm.hw3.part3.task4;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Contest {

    private final Person[] persons;
    private final Dog[] dogs;
    private final ArrayList<Participant> participants;
    private final double[][] rates;
    private final Scanner sc = new Scanner(System.in);

    public Contest() {
        int n = sc.nextInt();
        sc.nextLine();
        this.persons = new Person[n];
        this.dogs = new Dog[n];
        this.participants = new ArrayList<>();
        this.rates = new double[n][3];
        fillAll();
    }

    private void fillAll() {
        fillPersons();
        fillDogs();
        fillRates();
        fillParticipants();
    }

    private void fillPersons() {
        for (int i = 0; i < persons.length; i++) {
            persons[i] = new Person(sc.nextLine());
        }
    }

    private void fillDogs() {
        for (int i = 0; i < dogs.length; i++) {
            dogs[i] = new Dog(sc.nextLine());
        }
    }

    private void fillRates() {
        for (int i = 0; i < rates.length; i++) {
            for (int j = 0; j < rates[i].length; j++) {
                rates[i][j] = sc.nextDouble();
            }
        }
    }

    private void fillParticipants() {
        for (int i = 0; i < persons.length; i++) {
            participants.add(new Participant(persons[i], dogs[i], getListRate(i)));
        }
    }

    private ArrayList<Double> getListRate(int row) {
        ArrayList<Double> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(rates[row][i]);
        }
        return list;
    }

    public void showWinners() {
        sortByWinner();
        DecimalFormat df = new DecimalFormat("#.0");
        df.setRoundingMode(RoundingMode.DOWN);
        for (int i = 0; i < 3 && i < participants.size(); i++) {
            System.out.printf("%s: %s, %s\n",
                    participants.get(i).getPerson().getName(),
                    participants.get(i).getDog().getNickname(),
                    df.format(participants.get(i).avgRate()));
        }
    }

    private void sortByWinner() {
        participants.sort((o1, o2) -> {
            int result = 0;
            if (o1.avgRate() > o2.avgRate()) {
                result = -1;
            } else if (o2.avgRate() > o1.avgRate()) {
                result = 1;
            }
            return result;
        });
    }
}
