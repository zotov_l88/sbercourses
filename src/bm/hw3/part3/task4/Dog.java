package bm.hw3.part3.task4;

public class Dog {

    private final String nickname;

    public Dog(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }
}
