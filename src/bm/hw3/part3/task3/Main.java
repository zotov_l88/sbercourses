package bm.hw3.part3.task3;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static Scanner sc = new Scanner(System.in);
    static int N = sc.nextInt();
    static int M = sc.nextInt();

    public static void main(String[] args) {
        printArrayList(getList());
    }

    private static void printArrayList(ArrayList<ArrayList<Integer>> list) {
        for (ArrayList<Integer> integers : list) {
            for (int i = 0; i < integers.size(); i++) {
                System.out.printf("%d%s", integers.get(i), i < integers.size() - 1 ? " " : "\n");
            }
        }
    }

    private static ArrayList<ArrayList<Integer>> getList() {
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        ArrayList<Integer> tmp;
        for (int i = 0; i < M; i++) {
            tmp = new ArrayList<>();
            for (int j = 0; j < N; j++) {
                tmp.add(i + j);
            }
            list.add(tmp);
        }
        return list;
    }
}
