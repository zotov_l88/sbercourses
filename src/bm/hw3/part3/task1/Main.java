package bm.hw3.part3.task1;

import bm.hw3.part3.task1.abstracts.AbstractAnimal;
import bm.hw3.part3.task1.implementations.Bat;
import bm.hw3.part3.task1.implementations.Dolphin;
import bm.hw3.part3.task1.implementations.Eagle;
import bm.hw3.part3.task1.implementations.GoldFish;
import bm.hw3.part3.task1.interfaces.Flyable;
import bm.hw3.part3.task1.interfaces.Swimmable;

public class Main {
    public static void main(String[] args) {
        GoldFish goldFish = new GoldFish();
        Eagle eagle = new Eagle();
        Dolphin dolphin = new Dolphin();
        Bat bat = new Bat();

        AbstractAnimal[] animals = new AbstractAnimal[]{goldFish, eagle, dolphin, bat};
        for (AbstractAnimal animal : animals) {
            animal.wayOfBirth();
            animal.eat();
            animal.sleep();
        }

        for (Flyable flyable : new Flyable[]{eagle, bat}) {
            flyable.flying();
        }

        for (Swimmable swimmable : new Swimmable[]{goldFish, dolphin}) {
            swimmable.swimming();
        }

    }
}
