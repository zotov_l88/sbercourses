package bm.hw3.part3.task1.implementations;

import bm.hw3.part3.task1.abstracts.AbstractMammal;
import bm.hw3.part3.task1.interfaces.Swimmable;

public class Dolphin extends AbstractMammal implements Swimmable {

    @Override
    public void swimming() {
        System.out.println("The dolphin is swimming fast");
    }
}
