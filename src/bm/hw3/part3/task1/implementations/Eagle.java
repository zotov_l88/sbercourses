package bm.hw3.part3.task1.implementations;

import bm.hw3.part3.task1.abstracts.AbstractBird;
import bm.hw3.part3.task1.interfaces.Flyable;

public class Eagle extends AbstractBird implements Flyable {

    @Override
    public void flying() {
        System.out.println("The eagle is flying fast");
    }
}
