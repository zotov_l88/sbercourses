package bm.hw3.part3.task1.implementations;

import bm.hw3.part3.task1.abstracts.AbstractFish;
import bm.hw3.part3.task1.interfaces.Swimmable;

public class GoldFish extends AbstractFish implements Swimmable {

    @Override
    public void swimming() {
        System.out.println("The gold fish is swimming slowly");
    }
}
