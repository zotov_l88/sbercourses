package bm.hw3.part3.task1.implementations;

import bm.hw3.part3.task1.abstracts.AbstractMammal;
import bm.hw3.part3.task1.interfaces.Flyable;

public class Bat extends AbstractMammal implements Flyable {

    @Override
    public void flying() {
        System.out.println("The bat is flying slowly");
    }
}
