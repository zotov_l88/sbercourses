package bm.hw3.part3.task1.abstracts;

public abstract class AbstractAnimal {

    public abstract void wayOfBirth();

    public final void eat() { System.out.println("eat"); }

    public final void sleep() { System.out.println("sleep"); }

}
