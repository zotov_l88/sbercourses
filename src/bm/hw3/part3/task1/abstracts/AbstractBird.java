package bm.hw3.part3.task1.abstracts;

public abstract class AbstractBird extends AbstractAnimal {

    public final void wayOfBirth() {
        System.out.println("The bird was born");
    }

}
