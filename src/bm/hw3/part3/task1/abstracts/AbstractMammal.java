package bm.hw3.part3.task1.abstracts;

public abstract class AbstractMammal extends AbstractAnimal {

    public final void wayOfBirth() {
        System.out.println("The mammal was born");
    }

}
