package bm.hw3.part2;

import java.util.ArrayList;
import java.util.Objects;

public class Book {

    private final String author;
    private final String title;
    private int status;
    public ArrayList<Integer> listOfGrade;

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
        listOfGrade = new ArrayList<>();
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double avgRate() {
        double result = 0;
        for (Integer rate : listOfGrade) {
            result += rate;
        }
        return result == 0 ? 0 : result / listOfGrade.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(author, book.author) && Objects.equals(title, book.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, title);
    }

    @Override
    public String toString() {
        return "Книга {" +
                "Автор='" + author + '\'' +
                ", Название='" + title + '\'' +
                ", status=" + status +
                ", Оценки=" + listOfGrade +
                '}';
    }
}
