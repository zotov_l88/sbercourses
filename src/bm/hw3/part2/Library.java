package bm.hw3.part2;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

public class Library {

    private final ArrayList<Book> allBooks;
    private final HashMap<String, ArrayList<String>> sortByAuthorMap;
    private final HashMap<Visitor, Book> borrowedBookList;
    private static int ID;

    public Library() {
        this.allBooks = fillLibrary();
        this.sortByAuthorMap = sortByAuthor();
        this.borrowedBookList = new HashMap<>();
    }

    private ArrayList<Book> fillLibrary() {
        ArrayList<Book> list = new ArrayList<>();
        File file = new File("/home/user/IdeaProjects/sbercourses/src/bm.hw3/part2/books/books.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            String[] arr;
            while ((line = br.readLine()) != null) {
                arr = line.split(" — ");
                Book book = new Book(arr[0], arr[1]);
                book.setStatus(1);
                list.add(book);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    public boolean addBook(Book book) {
        boolean status = false;
        if (!allBooks.contains(book)) {
            allBooks.add(book);
            book.setStatus(1);
            status = true;
        }
        return status;
    }

    public boolean removeBookByTitle(String title) {
        boolean result = false;
        for (Book book : allBooks) {
            if (book.getTitle().equals(title)) {
                result = removeBook(book);
                break;
            }
        }
        return result;
    }

    public boolean removeBook(Book book) {
        Book tmpBook = findBook(book);
        boolean status = false;
        if (allBooks.contains(tmpBook) && tmpBook.getStatus() == 1) {
            allBooks.remove(tmpBook);
            status = true;
        }
        return status;
    }

    public Book getRandomBook() {
        return allBooks.get(new Random().nextInt(allBooks.size()));
    }

    public Visitor whereBook(Book book) {
        Visitor visitor = null;
        Book tmpBook = findBook(book);
        if (tmpBook != null) {
            for (Visitor vis : borrowedBookList.keySet()) {
                if (borrowedBookList.get(vis).equals(book)) {
                    visitor = vis;
                }
            }
        }
        return visitor;
    }

    public double rateOfBookByTitle(String title) {
        double rank = -1;
        Book book = findBookByTitle(title);
        if (book != null) {
            rank = Double.parseDouble(new DecimalFormat( "#.##" ).format(book.avgRate()));
        }
        return rank;
    }

    private HashMap<String, ArrayList<String>> sortByAuthor() {
        HashMap<String, ArrayList<String>> map = new HashMap<>();
        for (Book book : allBooks) {
            if (!map.containsKey(book.getAuthor())) {
                ArrayList<String> list = new ArrayList<>();
                list.add(book.getTitle());
                map.put(book.getAuthor(), list);
            } else {
                map.get(book.getAuthor()).add(book.getTitle());
                map.put(book.getAuthor(), map.get(book.getAuthor()));
            }
        }
        return map;
    }

    public void printAllBook() {
        for (Book book : allBooks) {
            System.out.printf("%s - %s\n", book.getAuthor(), book.getTitle());
        }
    }

    public Book getBookByNumber(int number) {
        if (number > 0 && number <= allBooks.size()) {
            return allBooks.get(number - 1);
        } else {
            throw new IllegalArgumentException("Всего книг: " + allBooks.size());
        }
    }

    // возвращает непосредственно тот объект книги, который хранится в библиотеке
    public Book findBook(Book book) {
        Book result = null;
        for (Book b : allBooks) {
            if (b.equals(book)) {
                result = b;
                break;
            }
        }
        return result;
    }

    public Book findBookByTitle(String title) {
        Book result = null;
        for (Book book : allBooks) {
            if (book.getTitle().equalsIgnoreCase(title)) {
                result = book;
                break;
            }
        }
        return result;
    }

    public void findBooksOfAuthor(String author) {
        int count = 1;
        if (sortByAuthorMap.containsKey(author)) {
            System.out.printf("Список книг автора %s:\n", author);
            for (String titleBook : sortByAuthorMap.get(author)) {
                System.out.printf("\t%d. %s\n", count++, titleBook);
            }
        } else {
            System.out.printf("Нет книг автора %s.\n", author);
        }
    }

    public int getStatusBook(Book book) {
        int status = -1;
        for (Book b : allBooks) {
            if (b.equals(book)) {
                status = b.getStatus();
            }
        }
        return status;
    }

    public void setStatusBook(Book book, int status) {
        for (Book b : allBooks) {
            if (b.equals(book)) {
                b.setStatus(status);
            }
        }
    }

    public boolean isBorrowBook(Visitor visitor, Book book) {
        boolean result = false;
        if (getStatusBook(book) == 1 && visitor.getBook() == null) {
            if (visitor.getID() == 0) {
                visitor.setID(++ID);
            }
            borrowedBookList.put(visitor, findBook(book));
            visitor.setBook(findBook(book));
            setStatusBook(book, 0);
            result = true;
        }
        return result;
    }

    public void borrowProcess(Visitor visitor, Book book) {
        if (book != null) {
            if (book.getStatus() == 1) {
                if (visitor.getBook() == null) {
                    isBorrowBook(visitor, book);
                    System.out.println("Книга одолжена.");
                } else {
                    System.out.println("Посетитель не вернул прошлую книгу.");
                }
            } else {
                System.out.println("Книга находится у " + whereBook(book).getName());
            }
        } else {
            System.out.println("Такой книги нет.");
        }
    }

    public boolean isPickUpBook(Visitor visitor, Book book) {
        boolean result = false;
        for (Visitor vis : borrowedBookList.keySet()) {
            if (vis.equals(visitor) && vis.getBook().equals(findBook(book))) {
                vis.setBook(null);
                borrowedBookList.get(vis).setStatus(1);
                borrowedBookList.remove(vis);
                result = true;
                break;
            }
        }
        return result;
    }

    public void pickUpBookProcess(Visitor visitor, Book book) {
        if (borrowedBookList.containsKey(visitor)) {
            if (borrowedBookList.get(visitor).equals(book)) {
                isPickUpBook(visitor, book);
                System.out.println("Забираем книгу у посетителя");
            } else {
                System.out.println("Посетитель одалживал не эту книгу");
            }

        } else {
            System.out.println("Не принимаем. У посетителя нет одолженных книг");
        }
    }

    public void info() {
        System.out.printf("%s: %d\n", "Всего книг в библиотеке", countOfAllBook());
        System.out.printf("%s: %d\n", "Книг в наличии", countOfBookIN());
        System.out.printf("%s: %d\n", "Книг у посетителей", countOfBookOUT());
        if (countOfBookOUT() > 0) {
            for (Visitor visitor : borrowedBookList.keySet()) {
                System.out.printf("\t%s, ID %d | %s - %s | средняя оценка - %.2f\n", visitor.getName(), visitor.getID(),
                        borrowedBookList.get(visitor).getAuthor(), borrowedBookList.get(visitor).getTitle(), visitor.getBook().avgRate());
            }
        }
    }

    private void printRateOfBooks(ArrayList<Book> list) {
        int count = 1;
        for (Book book : list) {
            System.out.printf("%d.%s - %s | средняя оценка: %.2f на основе %d голосов\n", count++,
                    book.getAuthor(), book.getTitle(), book.avgRate(), book.listOfGrade.size());
        }
    }

    public void rateOfBooks() {
        printRateOfBooks(allBooks);
    }

    public void rateOfBooksSorted() {
        ArrayList<Book> sorted = new ArrayList<>(allBooks);
        sorted.sort((o1, o2) -> {
            int result = 0;
            if (o1.avgRate() > o2.avgRate()) {
                result = -1;
            } else if (o2.avgRate() > o1.avgRate()) {
                result = 1;
            }
            return result;
        });
        printRateOfBooks(sorted);
    }

    public int countOfAllBook() {
        return allBooks.size();
    }

    public int countOfBookIN() {
        return countOfBookStatus(1);
    }

    public int countOfBookOUT() {
        return countOfBookStatus(0);
    }

    public int countOfBookStatus(int status) {
        int count = 0;
        for (Book book : allBooks) {
            count = book.getStatus() == status ? count + 1 : count;
        }
        return count;
    }
}
