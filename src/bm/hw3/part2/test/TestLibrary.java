package bm.hw3.part2.test;

import bm.hw3.part2.Book;
import bm.hw3.part2.Library;
import bm.hw3.part2.Visitor;

import java.util.Random;
import java.util.Scanner;

public class TestLibrary {

    static Library library = new Library();
    static Visitor[] visitors = {new Visitor("Viktor"), new Visitor("Boris"), new Visitor("Alexey"),
            new Visitor("Semen"), new Visitor("Viktor"), new Visitor("Sergey"),
            new Visitor("Sergey"), new Visitor("Julia"), new Visitor("Anna"),
            new Visitor("Olga"), new Visitor("Kristina"), new Visitor("Svetlana"),
            new Visitor("Maria"), new Visitor("Kira"), new Visitor("Svetlana"),
            new Visitor("Rita"), new Visitor("Ivan"), new Visitor("Vladimir"),
            new Visitor("Ekaterina"), new Visitor("Miron"), new Visitor("Natalya"),
    };

    public static void main(String[] args) {
        switch (new Scanner(System.in).nextInt()) {
            /*
            Имитация работы библиотеки, включающая 100_000 итераций с выводом списков книг с рейтингом в порядке
            добавления книг в библиотеку и в отсортированном по рейтингу.
             */
            case 1 -> test1();
            // Добавление и удаление книги в зависимости от наличия в библиотеке
            case 2 -> test2();
            // Вернуть книгу по названию и вернуть список книг автора
            case 3 -> test3();
            // Тестирование механизма одалживания книги
            case 4 -> test4();
            default -> System.out.println("1 - 5");
        }
    }

    private static void test4() {
        System.out.println("***************Выдаем***************");
        Book book1 = library.findBookByTitle("Война и мир");
        Book book2 = library.findBookByTitle("Евгений Онегин");
        System.out.println(book1);
        System.out.println(book2);
        library.borrowProcess(visitors[5], book1); // Выдаем книгу1. Статус книги меняется на 0
        System.out.println(visitors[5]);
        library.borrowProcess(visitors[7], book1); // Не выдаем, потому что книга у другого
        System.out.println(visitors[7]);
        library.borrowProcess(visitors[5], book2); // Не выдаем, потому что не вернул прошлую книгу
        System.out.println(visitors[5]);

        System.out.println("\n***************Забираем***************");
        library.borrowProcess(visitors[10], book2); // Выдаем книгу2 посетителю
        System.out.println(visitors[10]);
        library.pickUpBookProcess(visitors[7], book1); // Не принимаем, потому что у посетителя нет активных книг
        library.pickUpBookProcess(visitors[10], book1); // Не принимаем, потому что посетитель одалживал не эту книгу1
        library.pickUpBookProcess(visitors[5], book1); // Забираем книгу
        System.out.println(visitors[5]); // У клиента теперь нет книги
        System.out.println(book1); // Статус книги сменился на 1
    }

    private static void test3() {
        System.out.println(library.findBookByTitle("Дракула")); // книга есть в библиотеке
        System.out.println(library.findBookByTitle("Холодный кофе в жаркий полдень")); // выдуманная книга
        String author1 = "Илья Ильф, Евгений Петров";
        String author2 = "Кот Матроскин";
        library.findBooksOfAuthor(author1);
        library.findBooksOfAuthor(author2);
    }

    private static void test2() {
        library.info();
        System.out.println(library.addBook(new Book("AAAA", "BBBBB"))); // true
        library.info();
        System.out.println("Последняя добавленная книга: " + library.getBookByNumber(library.countOfAllBook()));

        System.out.println("************************");
        Book randomBook = library.getRandomBook();
        System.out.println("Random book - " + randomBook);
        library.borrowProcess(visitors[0], randomBook); // При передаче клиенту статус книги меняется с 1 - в наличии на 0 - отсутствует
        System.out.println(visitors[0]);
        // Добавление книги, которая уже есть в библиотеке даст false. Для корректной работы переопределен equals в классе Book
        System.out.println(library.addBook(new Book(randomBook.getAuthor(), randomBook.getTitle())));
        library.info();

        System.out.println("\n************Удаление по объекту************");
        // удаление книги со статусом 1. true
        System.out.println(library.removeBook(new Book("AAAA", "BBBBB")));
        System.out.println("Последняя добавленная книга: " + library.getBookByNumber(library.countOfAllBook()));
        // удаление книги со статусом 0. false
        System.out.println(library.removeBook(randomBook));
        library.info();

        System.out.println("\n************Удаление по названию************");
        Book book = library.getBookByNumber(12);
        System.out.println(book);
        library.borrowProcess(visitors[12], book);
        System.out.println(visitors[12]);
        System.out.println(library.removeBookByTitle(book.getTitle())); // false, потому что одолжена
        library.pickUpBookProcess(visitors[12], book); // вернули
        System.out.println(library.findBookByTitle(book.getTitle())); // Ищем книгу и находим в списке
        System.out.println(library.removeBookByTitle(book.getTitle())); // true
        System.out.println(library.findBookByTitle(book.getTitle())); // Снова ищем и не находим null
    }

    public static void test1() {
        Random random = new Random();
        for (int i = 0; i < 100_000; i++) {
            System.out.println("*****************" + i + "*****************");
            Book randBook = library.getRandomBook();
            Visitor randVisitor = visitors[random.nextInt(visitors.length)];
            if (random.nextBoolean()) {
                System.out.printf("%s\n", library.isBorrowBook(randVisitor, randBook) ?
                        "Выдаем книгу" : "У посетителя уже есть книга");
            } else {
                randVisitor.rateBook(random.nextInt(10) + 1);
                System.out.printf("%s\n", library.isPickUpBook(randVisitor, randVisitor.getBook()) ?
                        "Забираем книгу" : "Нечего забирать");
            }
            System.out.println(randBook);
            System.out.println(randVisitor);
            library.info();
        }
        System.out.println("\n*****************" + "Рейтинг книг" + "*****************");
        library.rateOfBooks();

        System.out.println("\n*****************" + "Рейтинг книг сортированный" + "*****************");
        library.rateOfBooksSorted();

        System.out.println("\n*****************" + "Рейтинг книги" + "*****************");
        System.out.printf("Рейтинг книги %s - %s: %.2f\n",
                library.findBookByTitle("Понедельник начинается в субботу").getAuthor(),
                library.findBookByTitle("Понедельник начинается в субботу").getTitle(),
                library.rateOfBookByTitle("Понедельник начинается в субботу"));

    }
}
