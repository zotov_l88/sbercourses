package bm.hw3.part2;

import java.util.HashMap;
import java.util.Objects;

public class Visitor {

    private final String name;
    private int ID;
    private Book book;
    private final HashMap<Book, Integer> listOfRated;

    public Visitor(String name) {
        this.name = name;
        listOfRated = new HashMap<>();
    }

    public void rateBook(int rate) {
        if (rate > 0 && rate < 11) {
            if (book != null) {
                checkRate(rate);
            } else {
                System.out.println("Нет книги для оценивания.");
            }
        } else {
            System.out.println("Оценка может быть в диапазоне от 1 до 10 включительно.");
        }
    }

    private void checkRate(int rate) {
        if (listOfRated.containsKey(book)) {
            book.listOfGrade.remove(listOfRated.get(book));
            book.listOfGrade.add(rate);
            System.out.println("Оценка книги изменена.");
        } else {
            book.listOfGrade.add(rate);
            System.out.println("Оценка добавлена.");
        }
        listOfRated.put(book, rate);
    }

    public String getName() {
        return name;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visitor visitor = (Visitor) o;
        return ID == visitor.ID && Objects.equals(name, visitor.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, ID);
    }

    @Override
    public String toString() {
        return "Посетитель {" +
                "Имя='" + name + '\'' +
                ", ID=" + ID +
                ", " + book +
                '}';
    }
}
