package bm.hw2.part2;

/*
Подсчет калорий
Петя решил начать следить за своей фигурой.
Но все существующие приложения для подсчета калорий ему не понравились и он решил написать свое.
Петя хочет каждый день записывать сколько белков, жиров, углеводов и калорий он съел, а в конце недели приложение должно его уведомлять, вписался ли он в свою норму или нет.

На вход подаются числа A — недельная норма белков, B — недельная норма жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было съедено Петей нутриентов в каждый день недели.
Если за неделю в сумме по каждому нутриенту не превышена недельная норма, то вывести “Отлично”, иначе вывести “Нужно есть поменьше”.

Ограничение:
0 < A, B, C < 2000
0 < ai, bi, ci < 2000
0 < K < 20000
0 < ki < 20000
 */

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        System.out.println(isWeekNorm(initCalCalendar()) ? "Отлично" : "Нужно есть поменьше");
    }

    private static boolean isWeekNorm(int[][] matrix) {
        int count = matrix[0].length - 1;
        boolean isNormal = true;
        while (count >= 0) {
            if (sumCol(matrix, count) > matrix[0][count]) {
                isNormal = false;
                break;
            }
            count--;
        }
        return isNormal;
    }

    private static int sumCol(int[][] matrix, int index) {
        int sum = 0;
        int count = matrix.length - 1;
        while (count >= 1) {
            sum += matrix[count][index];
            count--;
        }
        return sum;
    }

    private static int[][] initCalCalendar() {
        Scanner sc = new Scanner(System.in);
        int[][] matrix = new int[8][4];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        return matrix;
    }
}
