package bm.hw2.part2;

/*
Нарисовать прямоугольник
На вход подается число N — количество строк и столбцов матрицы.
Затем в последующих двух строках подаются координаты X (номер столбца) и Y (номер строки) точек, которые задают прямоугольник.

Необходимо отобразить прямоугольник с помощью символа 1 в матрице, заполненной нулями (см. пример) и вывести всю матрицу на экран.

Ограничение:
0 < N < 100
0 <= X1, Y1, X2, Y2 < N
X1 < X2
Y1 < Y2
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int[][] matrix = new int[m][m];
        int x1 = sc.nextInt();
        int y1 = sc.nextInt();
        int x2 = sc.nextInt();
        int y2 = sc.nextInt();

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = isRange(i, j, x1, y1, x2, y2) ? 1 : 0;
                System.out.print(matrix[i][j] + ((j < m - 1) ? " " : ""));
            }
            System.out.printf("%s", (i < m - 1) ? '\n' : "");
        }
    }

    private static boolean isRange(int i, int j, int x1, int y1, int x2, int y2) {
        return (j >= x1 && j <= x2 && (i == y1 || i == y2)) ||
                (i >= y1 && i <= y2 && (j == x1 || j == x2));
    }
}