package bm.hw2.part2;

/*
Удаление из матрицы
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
После этого передается натуральное число P.

Необходимо найти элемент P в матрице и удалить столбец и строку его содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.

Ограничение:
0 < N < 100
0 < ai < 1000
 */

import java.util.Scanner;

public class Task4 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] matrix = initMatrix();
        Coordinate coordinate = getCoordinate(matrix, sc.nextInt());
        if (coordinate != null) {
            printMatrixWithoutColsAndRows(matrix, coordinate);
        }
    }

    private static void printMatrixWithoutColsAndRows(int[][] matrix, Coordinate coordinate) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i != coordinate.getY()) {
                    if (j != coordinate.getX()) {
                        System.out.print(matrix[i][j]);
                    }
                    if (j + 1 != coordinate.getX() && j < matrix[i].length - 1) {
                        System.out.print(" ");
                    }
                }
            }
            if (i + 1 != coordinate.getY() && i < matrix.length - 1) {
                System.out.println();
            }
        }
    }

    private static Coordinate getCoordinate(int[][] matrix, int val) {
        Coordinate coordinate = null;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == val) {
                    coordinate = new Coordinate(j, i);
                }
            }
        }
        return coordinate;
    }

    private static int[][] initMatrix() {
        int m = sc.nextInt();
        int[][] matrix = new int[m][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        return matrix;
    }

    static class Coordinate {

        private final int x;
        private final int y;

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}

