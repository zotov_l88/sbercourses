package bm.hw2.part2;
/*
Минимальный элемент
На вход передается N — количество столбцов в двумерном массиве и M — количество строк.
Затем сам передается двумерный массив, состоящий из натуральных чисел.

Необходимо сохранить в одномерном массиве и вывести на экран минимальный элемент каждой строки.

Ограничение:
0 < N < 100
0 < M < 100
0 < ai < 1000
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[][] array = new int[m][n];
        int[] minElements = new int[m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        int index = 0;
        for (int i = 0; i < m; i++) {
            minElements[index] = array[i][0];
            for (int j = 0; j < n; j++) {
                if (minElements[index] > array[i][j]) {
                    minElements[index] = array[i][j];
                }
            }
            index++;
        }
        for (int element : minElements) {
            System.out.print(element + " ");
        }
    }
}
