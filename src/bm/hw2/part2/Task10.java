package bm.hw2.part2;

/*
Обратный порядок
На вход подается число N.
Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.

Ограничение:
0 < N < 1000000
 */

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        System.out.println(digitsOfNumberRightToLeft(new Scanner(System.in).nextLine()));
    }

    private static String digitsOfNumberRightToLeft(String number) {
        if (number.length() == 0) {
            return "";
        }
        return number.substring(number.length() - 1) + " " +
                digitsOfNumberRightToLeft(number.substring(0, number.length() - 1));
    }
}
