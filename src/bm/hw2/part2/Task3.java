package bm.hw2.part2;

/*
Шахматы
На вход подается число N — количество строк и столбцов матрицы.
Затем передаются координаты X и Y расположения коня на шахматной доске.

Необходимо заполнить матрицу размера NxN нулями, местоположение коня отметить символом K,
а позиции, которые он может бить, символом X.

Ограничение:
4 < N < 100
0 <= X, Y < N
 */

import java.util.Scanner;

public class Task3 {

    static Scanner sc = new Scanner(System.in);
    static final char EMPTY = '0';
    static final char HORSE = 'K';
    static final char STEP = 'X';

    public static void main(String[] args) {
        char[][] field = initMatrix();
        int y = sc.nextInt();
        int x = sc.nextInt();
        field[x][y] = HORSE;
        checkStepsHorse(field, x, y);
        printMatrix(field);
    }

    private static void printMatrix(char[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%s%s", matrix[i][j], (j < matrix[i].length - 1)? " " : "");
            }
            System.out.printf("%s", i < matrix.length - 1 ? '\n' : "");
        }
    }

    private static void checkStepsHorse(char[][] matrix, int x, int y) {
        checkXaxis(matrix, x, y);
        checkYaxis(matrix, x, y);
    }

    private static void checkXaxis(char[][] matrix, int x, int y) {
        if (x - 2 >= 0) {
            if (y - 1 >= 0) {
                matrix[x - 2][y - 1] = STEP;
            }
            if (y + 1 < matrix[x].length) {
                matrix[x - 2][y + 1] = STEP;
            }
        }
        if (x + 2 < matrix.length) {
            if (y - 1 >= 0) {
                matrix[x + 2][y - 1] = STEP;
            }
            if (y + 1 < matrix[x].length) {
                matrix[x + 2][y + 1] = STEP;
            }
        }
    }

    private static void checkYaxis(char[][] matrix, int x, int y) {
        if (y - 2 >= 0) {
            if (x - 1 >= 0) {
                matrix[x - 1][y - 2] = STEP;
            }
            if (x + 1 < matrix.length) {
                matrix[x + 1][y - 2] = STEP;
            }
        }
        if (y + 2 < matrix[x].length) {
            if (x - 1 >= 0) {
                matrix[x - 1][y + 2] = STEP;
            }
            if (x + 1 < matrix.length) {
                matrix[x + 1][y + 2] = STEP;
            }
        }
        
    }

    private static char[][] initMatrix() {
        int m = sc.nextInt();
        char[][] matrix = new char[m][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = EMPTY;
            }
        }
        return matrix;
    }
}
