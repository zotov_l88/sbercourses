package bm.hw2.part2;

/*
Сумма цифр
На вход подается число N.
Необходимо посчитать и вывести на экран сумму его цифр.
Решить задачу нужно через рекурсию.

Ограничение:
0 < N < 1000000
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        System.out.println(sumDigits(new Scanner(System.in).nextInt()));
    }

    private static int sumDigits(int n) {
        if (n == 0) {
            return 0;
        }
        return n % 10 + sumDigits(n / 10);
    }
}
