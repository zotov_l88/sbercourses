package bm.hw2.part2;

/*
Вывести цифры
На вход подается число N.
Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.

Ограничение:
0 < N < 1000000
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        System.out.println(digitsOfNumber(new Scanner(System.in).nextLine()));
    }

    private static String digitsOfNumber(String number) {
        if (number.length() == 0) {
            return "";
        }
        return number.substring(0, 1) + " " + digitsOfNumber(number.substring(1));
    }
}
