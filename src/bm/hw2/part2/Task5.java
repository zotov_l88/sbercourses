package bm.hw2.part2;

/*
Проверить симметричность
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.

Необходимо вывести true, если она является симметричной относительно побочной диагонали, false иначе.

Побочной диагональю называется диагональ, проходящая из верхнего правого угла в левый нижний.

Ограничение:
0 < N < 100
0 < ai < 1000
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        System.out.println(isMatrixSymmetrySideDiagonal(initMatrix()));
    }

    private static boolean isMatrixSymmetrySideDiagonal(int[][] matrix) {
        boolean isSame = true;
        int startI = 0;
        int startJ = 0;
        for (int i = matrix.length - 1; i > 0; i--) {
            for (int j = matrix[i].length - 1; j >= i; j--) {
                int n = matrix[j][i];
                int m = matrix[startI][startJ++];
                if (n != m) {
                    isSame = false;
                    break;
                }
            }
            if (isSame) {
                break;
            }
            startJ = 0;
            startI++;
        }
        return isSame;
    }

    private static int[][] initMatrix() {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int[][] matrix = new int[m][m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }
        return matrix;
    }
}