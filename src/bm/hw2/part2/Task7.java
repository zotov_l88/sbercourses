package bm.hw2.part2;

/*
Конкурс красоты
Раз в год Петя проводит конкурс красоты для собак.
К сожалению, система хранения участников и оценок неудобная, а победителя определить надо.
В первой таблице в системе хранятся имена хозяев, во второй - клички животных, в третьей — оценки трех судей за выступление каждой собаки.
Таблицы связаны между собой только по индексу.
То есть хозяин i-ой собаки указан в i-ой строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы.
Нужно помочь Пете определить топ 3 победителей конкурса.

На вход подается число N — количество участников конкурса. Затем в N строках переданы имена хозяев.
После этого в N строках переданы клички собак.
Затем передается матрица с N строк, 3 вещественных числа в каждой — оценки судей.
Победителями являются три участника, набравшие максимальное среднее арифметическое по оценкам 3 судей.
Необходимо вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Среднюю оценку выводить с точностью один знак после запятой.

Гарантируется, что среднее арифметическое для всех участников будет различным.

Ограничение:
0 < N < 100
 */

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Task7 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Participant[] participants = new Participant[sc.nextInt()];
        initParticipants(participants);
        sortParticipants(participants);
        getWinners(participants);
    }

    private static void getWinners(Participant[] participants) {
        DecimalFormat df = new DecimalFormat("#.0");
        df.setRoundingMode(RoundingMode.DOWN);
        for (int i = 0; i < 3; i++) {
            System.out.printf("%s: %s, %s%s",
                    participants[i].name, participants[i].pet, df.format(participants[i].score),
                    i < 2 ? "\n" : "");
        }
    }

    private static void sortParticipants(Participant[] participants) {
        for (int i = 0; i < participants.length; i++) {
            boolean isSorted = true;
            for (int j = 0; j < participants.length - 1 - i; j++) {
                if (participants[j].score < participants[j + 1].score) {
                    swap(participants, j, j + 1);
                    isSorted = false;
                }
            }
            if (isSorted) {
                break;
            }
        }
    }

    private static void swap(Participant[] participants, int ind1, int ind2) {
        Participant tmp = participants[ind1];
        participants[ind1] = participants[ind2];
        participants[ind2] = tmp;
    }

    private static void initParticipants(Participant[] participants) {
        for (int i = 0; i < participants.length; i++) {
            participants[i] = new Participant();
        }
        for (Participant participant : participants) {
            participant.name = sc.next();
        }
        for (Participant participant : participants) {
            participant.pet = sc.next();
        }
        for (Participant participant : participants) {
            participant.score = (sc.nextDouble() + sc.nextDouble() + sc.nextDouble()) / 3;
        }
    }

    static class Participant {

        private String name = "";
        private String pet = "";
        private double score = 0;
    }
}
