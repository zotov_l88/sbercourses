package bm.hw2.part1;

/*
Найти ближайшее
На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
После этого передается число M.

Необходимо найти в массиве число, максимально близкое к M (т.е. такое число, для которого |ai - M| минимальное).
Если их несколько, то вывести максимальное число.

Ограничение:
0 < N < 100
-1000 < ai < 1000
-1000 < M < 1000
 */

import java.util.Scanner;

public class Task8 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = initArray();
        int m = sc.nextInt();
        if (array.length > 0) {
            System.out.println(minDifferent(array, m));
        }
    }

    private static int minDifferent(int[] array, int val) {
        int different = absDiff(array, 0, val);
        int result = array[0];
        for (int i = 1; i < array.length; i++) {
            int curDif = absDiff(array, i, val);
            if (curDif <= different) {
                different = curDif;
                if (result < array[i]) {
                    result = array[i];
                }
            }
        }
        return result;
    }

    private static int absDiff(int[] array, int i, int val) {
        int diff = 0;
        int arrV = array[i];
        if (val < 0) {
            if (arrV < 0) {
                diff = (val > arrV) ?  val - arrV : arrV - val;
            } else {
                diff = arrV - val;
            }
        } else {
            if (arrV < 0) {
                diff = val - arrV;
            } else {
                diff = (val > arrV) ? val - arrV : arrV - val;
            }
        }
        return diff;
    }

    private static int[] initArray() {
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }
}
