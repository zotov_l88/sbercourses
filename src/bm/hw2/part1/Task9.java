package bm.hw2.part1;

/*
Найти дубликат
На вход подается число N — длина массива.
Затем передается массив строк из N элементов (разделение через перевод строки).
Каждая строка содержит только строчные символы латинского алфавита.

Необходимо найти и вывести дубликат на экран.
Гарантируется что он есть и только один.

Ограничение:
0 < N < 100
0 < ai.length() < 1000
 */

import java.util.HashSet;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
        HashSet<String> set = new HashSet<>();
        while (sc.hasNext()) {
            String str = sc.nextLine();
            if (!set.add(str)) {
                System.out.println(str);
                break;
            }
        }
    }
}
