package bm.hw2.part1;

/*
Количество различных элементов
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо вывести на экран построчно сколько встретилось различных элементов.
Каждая строка должна содержать количество элементов и сам элемент через пробел.

Ограничение:
0 < N < 100
-1000 < ai < 1000
 */

import java.util.HashMap;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int key = sc.nextInt();
            if (!map.containsKey(key)) {
                map.put(key, 1);
            } else {
                map.put(key, map.get(key) + 1);
            }
        }
        for (Integer integer : map.keySet()) {
            System.out.println(map.get(integer) + " " + integer);
        }
    }
}
