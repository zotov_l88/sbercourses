package bm.hw2.part1;

/*
Возведение в квадрат
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного возведением в квадрат каждого элемента,
упорядочить элементы по возрастанию и вывести их на экран.

Ограничение:
0 < N < 100
-1000 < ai < 1000
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        int[] array = initArray();
        powArray(array);
        bubbleSort(array);
        printArray(array);
    }

    private static int[] initArray() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }

    private static void powArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * array[i];
        }
    }

    private static void bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean isSort = true;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(array, j, j + 1);
                    isSort = false;
                }
            }
            if (isSort) {
                break;
            }
        }
    }

    private static void swap(int[] array, int ind_1, int ind_2) {
        int tmp = array[ind_1];
        array[ind_1] = array[ind_2];
        array[ind_2] = tmp;
    }

    private static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
    }
}
