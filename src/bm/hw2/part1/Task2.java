package bm.hw2.part1;

/*
Равенство массивов
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого аналогично передается второй массив (aj) длины M.

Необходимо вывести на экран true, если два массива одинаковы
(то есть содержат одинаковое количество элементов и для каждого i == j элемент ai == aj).
Иначе вывести false.
 */

import java.util.Scanner;

public class Task2 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array1 = initArray();
        int[] array2 = initArray();
        System.out.println(isArraysSame(array1, array2));
    }

    private static boolean isArraysSame(int[] array1, int[] array2) {
        boolean result = true;
        if (array1.length != array2.length) {
            result = false;
        } else {
            for (int i = 0; i < array1.length; i++) {
                if (array1[i] != array2[i]) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    private static int[] initArray() {
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }
}
