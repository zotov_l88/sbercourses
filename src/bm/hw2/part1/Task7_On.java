package bm.hw2.part1;

/*
Возведение в квадрат
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.

Необходимо создать массив, полученный из исходного возведением в квадрат каждого элемента,
упорядочить элементы по возрастанию и вывести их на экран.

!!! Нужно решить за линейное время !!!

Ограничение:
0 < N < 100
-1000 < ai < 1000
 */

import java.util.Scanner;

public class Task7_On {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int[] arrayPositive = new int[N];
        int[] arrayNegative = new int[N];
        int realLengthAP = 0;
        int realLengthAN = 0;
        int i, j;
        StringBuilder sb = new StringBuilder();

        for (i = 0, j = 0; i + j < N; ) {
            int num = sc.nextInt();
            if (num < 0) {
                arrayNegative[j++] = num;
                realLengthAN++;
            } else {
                arrayPositive[i++] = num;
                realLengthAP++;
            }
        }

        for (i = 0, j = realLengthAN - 1; i < realLengthAP && j >= 0; ) {
            int positivePow = arrayPositive[i] * arrayPositive[i];
            int negativePow = arrayNegative[j] * arrayNegative[j];
            if (positivePow < negativePow) {
                i++;
                sb.append(positivePow).append(" ");
            } else {
                j--;
                sb.append(negativePow).append(" ");
            }
        }

        while (i < realLengthAP) {
            sb.append(arrayPositive[i] * arrayPositive[i]).append(" ");
            i++;
        }

        while (j >= 0) {
            sb.append(arrayNegative[j] * arrayNegative[j]).append(" ");
            j--;
        }
        System.out.print(sb.toString().trim());
    }
}
