package bm.hw2.part1;

/*
Азбука Морзе
На вход подается строка S, состоящая только из русских заглавных букв (без Ё).

Необходимо реализовать метод, который кодирует переданную строку с помощью азбуки Морзе и затем вывести
результат на экран. Отделять коды букв нужно пробелом.

Для удобства представлен массив с кодами Морзе ниже:
{".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-",
".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....",
"-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"}
 */

import java.util.Scanner;

public class Task6 {

    static final int SHIFT = 1040;
    static String[] morse = {
            ".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---",
            "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....",
            "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"
    };

    public static void main(String[] args) {
       System.out.println(convertStringToMorse(new Scanner(System.in).nextLine()));
    }

    private static String convertStringToMorse(String word) {
        StringBuilder sb = new StringBuilder();
        int count = 0;
        while (count < word.length()) {
            int n = word.charAt(count);
            sb.append(morse[n - SHIFT]);
            sb.append((count < word.length() - 1) ? " " : "");
            count++;
        }
        return sb.toString();
    }
}
