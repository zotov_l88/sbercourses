package bm.hw2.part1;

/*
Посчитать среднее арифметическое
На вход подается число N — длина массива.
Затем передается массив вещественных чисел (ai) из N элементов.


Необходимо реализовать метод, который принимает на вход полученный массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.

Ограничение:
0 < N < 100
0 < ai < 1000
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double[] array = new double[sc.nextInt()];
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextDouble();
            sum += array[i];
        }
        System.out.println(sum / array.length);
    }
}
