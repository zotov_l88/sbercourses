package bm.hw2.part1;

/*
Сдвиг
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов.
После этого передается число M — величина сдвига.

Необходимо циклически сдвинуть элементы массива на M элементов вправо.

Ограничение:
0 < N < 100
-1000 < ai < 1000
0 <= M < 100
 */

import java.util.Scanner;

public class Task5 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = initArray();
        int m = sc.nextInt();
        shiftArray(array, m);
        for (int i : array) {
            System.out.print(i + " ");
        }
    }

    private static void shiftArray(int[] array, int shift) {
        for (int i = 0; i < shift; i++) {
            int tmp = array[array.length - 1];
            for (int j = array.length - 1; j >= 1; j--) {
                array[j] = array[j - 1];
            }
            array[0] = tmp;
        }
    }

    private static int[] initArray() {
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }
}
