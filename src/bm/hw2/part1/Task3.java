package bm.hw2.part1;

/*
Найти индекс
На вход подается число N — длина массива.
Затем передается массив целых чисел (ai) из N элементов, отсортированный по возрастанию.
После этого вводится число X — элемент, который нужно добавить в массив, чтобы сортировка в массиве сохранилась.


Необходимо вывести на экран индекс элемента массива, куда нужно добавить X.
Если в массиве уже есть число равное X, то X нужно поставить после уже существующего.

Ограничение:
0 < N < 100
-1000 < ai < 1000
-1000 < X < 1000
 */

import java.util.Scanner;

public class Task3 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = initArray();
        int size = sc.nextInt();
        System.out.println(insertElement(array, size, 0, array.length - 1));
    }

    private static int insertElement(int[] array, int element, int start, int end) {
        int middle = 0;
        while (end >= start) {
            middle = (start + end) / 2;
            if (array[middle] == element) {
                middle = offset(array, middle, element);
                return middle;
            }
            if (array[middle] > element) {
                end = middle - 1;
            }
            if (array[middle] < element) {
                start = middle + 1;
            }
        }
        middle = middle == -1 ? 0 : middle;
        return array[middle] < element ? middle + 1 : middle;
    }

    private static int offset(int[] array, int index, int element) {
        int count = index;
        while (count < array.length) {
            if (array[count] != element) {
                break;
            }
            count++;
        }
        return count;
    }

    private static int[] initArray() {
        int n = sc.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }
}

