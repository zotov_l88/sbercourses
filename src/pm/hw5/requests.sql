--1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select *
from orders o join clients c on o.client_id = c.id join flowers f on o.flower_id = f.id
where o.id = 7;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select *
from orders o join clients c on o.client_id = c.id join flowers f on o.flower_id = f.id
where c.id = 2
      and data_order > now() - interval '1 month';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select flower as Название, max(quantity) as Количество
from orders o join flowers f on o.flower_id = f.id
group by flower
ORDER BY Количество desc
limit 1;

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(price * quantity) as Выручка
from orders o join flowers f on o.flower_id = f.id;

-- далее для тренировки придумал сам
--5. Вывести количество проданных цветов по каждой виду, цену за 1 штуку и за все
select flower, sum(quantity) as quantity, price, sum(quantity) * price as result
from orders o join flowers f on o.flower_id = f.id
group by flower, price
order by result desc

--6. Количество всего купленных цветов для каждого клиента
select name_buyer as name, sum(quantity) as куплено
from orders o join clients c on o.client_id = c.id join flowers f on o.flower_id = f.id
group by name_buyer
order by куплено desc;

--7. Потраченные деньги каждым клиентом
select name_buyer as name, sum(quantity * price) as потрачено
from orders o join clients c on o.client_id = c.id join flowers f on o.flower_id = f.id
group by name_buyer
order by потрачено desc;

--8. Количество купленных цветов каждой категории каждым клиентом за все время
select name_buyer, flower, sum(quantity) as quantity, (sum(quantity) * price) as spent
from orders o join clients c on o.client_id = c.id join flowers f on o.flower_id = f.id
group by name_buyer, flower, price
order by name_buyer;