create table clients (
    id serial primary key,
    name_buyer varchar(30) not null,
    phone varchar(30) not null
);

insert into clients
values (nextval('clients_id_seq'), 'Алексей', '+79091112233'),
        (nextval('clients_id_seq'), 'Сергей', '+79094445566'),
        (nextval('clients_id_seq'), 'Иван', '+79163330099'),
        (nextval('clients_id_seq'), 'Надежда', '+79179990909');
