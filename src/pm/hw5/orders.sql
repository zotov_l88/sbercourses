create table orders (
    id serial primary key,
    flower_id integer references flowers(id),
    client_id integer references clients(id),
    quantity integer check (quantity >= 1 and quantity <= 1000),
    data_order timestamp not null
);

insert into orders
values
(nextval('orders_id_seq'), 1, 2, 12, 'Mar 15, 2021, 1:05:01 PM'),
(nextval('orders_id_seq'), 3, 3, 3, 'Dec 13, 2022, 1:05:01 PM'),
(nextval('orders_id_seq'), 2, 4, 11, 'Jun 11, 2022, 1:05:01 PM'),
(nextval('orders_id_seq'), 1, 4, 7, 'Sep 22, 2021, 1:05:01 PM'),
(nextval('orders_id_seq'), 1, 1, 5, now()),
(nextval('orders_id_seq'), 2, 4, 17, now()),
(nextval('orders_id_seq'), 1, 2, 11, now()),
(nextval('orders_id_seq'), 3, 3, 19, now()),
(nextval('orders_id_seq'), 1, 1, 7, now()),
(nextval('orders_id_seq'), 1, 3, 15, now()),
(nextval('orders_id_seq'), 2, 2, 11, now()),
(nextval('orders_id_seq'), 1, 2, 21, now()),
(nextval('orders_id_seq'), 3, 4, 16, now()),
(nextval('orders_id_seq'), 2, 3, 1, now()),
(nextval('orders_id_seq'), 1, 3, 3, now()),
(nextval('orders_id_seq'), 1, 1, 7, now()),
(nextval('orders_id_seq'), 3, 4, 30, now());




