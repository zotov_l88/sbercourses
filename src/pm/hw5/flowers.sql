create table flowers (
    id serial primary key,
    flower varchar(30) not null,
    price int not null
);

insert into flowers
values (nextval('flowers_id_seq'), 'Роза', 100),
        (nextval('flowers_id_seq'), 'Лилия', 50),
        (nextval('flowers_id_seq'), 'Ромашка', 25);
