package pm.hw1.task4;

public class MyEvenNumber {

    private static int counter;

    private int evenNumber;

    public MyEvenNumber(int number) {
        if (isEven(number)) {
            evenNumber = number;
            counter++;
        }
    }

    public int getEvenNumber() {
        return evenNumber;
    }

    public static void infoInstance() {
        System.out.printf("Созданных отбъектов: %d\n", counter);
    }

    public boolean isEven(int number) throws MyEvenNumberException {
        if (number % 2 == 0) {
            return true;
        } else {
            throw new MyEvenNumberException(number + " is not a even number");
        }
    }
}
