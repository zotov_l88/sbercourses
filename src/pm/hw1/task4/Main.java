package pm.hw1.task4;

public class Main {
    public static void main(String[] args) {

        // первые два случая выбросят исключение
        new MyEvenNumber(5);
        new MyEvenNumber(-11);
        new MyEvenNumber(4);
        new MyEvenNumber(0);
        new MyEvenNumber(-22);
        MyEvenNumber.infoInstance();

    }
}
