package pm.hw1.task4;

public class MyEvenNumberException extends RuntimeException {

    public MyEvenNumberException(String message) {
        super(message);
    }
}
