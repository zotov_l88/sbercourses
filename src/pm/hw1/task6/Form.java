package pm.hw1.task6;

public class Form {

    private final String name;
    private final String birthdate;
    private final String gender;
    private final String height;

    public Form(String name, String birthdate, String gender, String height) {
        FormValidator.allChecks(name, birthdate, gender, height);
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.height = height;
        System.out.println("Анкета успешно заполнена");
    }

    public String getName() {
        return name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getGender() {
        return gender;
    }

    public String getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Form{" +
                "name='" + name + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", gender='" + gender + '\'' +
                ", height='" + height + '\'' +
                '}';
    }
}
