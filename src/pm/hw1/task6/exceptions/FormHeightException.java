package pm.hw1.task6.exceptions;

public class FormHeightException extends FormValidationException {

    public FormHeightException(String message) {
        super(message);
    }

    public FormHeightException() {
        super("Значение роста должно быть больше 0. Для разделителя используйте знак '.'");
    }
}
