package pm.hw1.task6.exceptions;

public class FormNameException extends FormValidationException {

    public FormNameException(String message) {
        super(message);
    }

    public FormNameException() {
        super("Длина имени должна быть от 2 до 20 символов, первая буква заглавная");
    }
}
