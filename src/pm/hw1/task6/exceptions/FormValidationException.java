package pm.hw1.task6.exceptions;

public class FormValidationException extends RuntimeException {

    public FormValidationException(String message) {
        super(message);
    }
}
