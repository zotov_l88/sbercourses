package pm.hw1.task6.exceptions;

public class FormGenderException extends FormValidationException {

    public FormGenderException(String message) {
        super(message);
    }

    public FormGenderException() {
        super("Male или Female");
    }
}
