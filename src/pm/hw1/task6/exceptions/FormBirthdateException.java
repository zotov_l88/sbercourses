package pm.hw1.task6.exceptions;

public class FormBirthdateException extends FormValidationException {

    public FormBirthdateException(String message) {
        super(message);
    }

    public FormBirthdateException() {
        super("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
    }
}
