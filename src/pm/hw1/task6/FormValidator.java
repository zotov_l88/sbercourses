package pm.hw1.task6;

import pm.hw1.task6.exceptions.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public final class FormValidator {

    private static final int[] DAYS_OF_LEAP_YEAR = new int[]{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private static final int[] DAYS_OF_NOT_LEAP_YEAR = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public static final String REG_BIRTHDATE = "^([1-9]|0[1-9]|[12][0-9]|3[01])\\.([1-9]|0[1-9]|1[0-2])\\.[12][09][0-9][0-9]$";
    public static final String REG_HEIGHT = "^[+]?\\d*\\.?\\d+$";

    private FormValidator() {
    }

    public static void allChecks(String name, String date, String gender, String height) throws FormValidationException {
        checkName(name);
        checkBirthdate(date);
        checkGender(gender);
        checkHeight(height);
    }

    public static void checkName(String str) throws FormNameException {
        if (str == null || !((str.length() > 1 && str.length() < 21) && (str.charAt(0) >= 'A' && str.charAt(0) <= 'Z'))) {
            throw new FormNameException();
        }
    }

    public static void checkBirthdate(String str) throws FormBirthdateException {
        boolean isCorrect = false;
        if (str != null && Pattern.matches(REG_BIRTHDATE, str)) {
            int day = Integer.parseInt(str.substring(0, str.indexOf('.')));
            int month = Integer.parseInt(str.substring(str.indexOf('.') + 1, str.lastIndexOf('.')));
            int year = Integer.parseInt(str.substring(str.lastIndexOf('.') + 1));
            isCorrect = isCorrectNumberOfDay(str, day, month, year);
        }
        if (!isCorrect) {
            throw new FormBirthdateException();
        }
    }
    public static void checkGender(String str) throws FormGenderException {
        if (str == null || !isRightGender(str)) {
            throw new FormGenderException();
        }
    }

    public static void checkHeight(String str) throws FormHeightException {
        if (str == null || (!(Pattern.matches(REG_HEIGHT, str) && Double.parseDouble(str) != 0))) {
            throw new FormHeightException();
        }
    }

    private static boolean isCorrectNumberOfDay(String str, int day, int month, int year) {
        return ((isLeapYear(year) && isCorrectNumberOfDayLeapYear(day, month)) ||
                (!isLeapYear(year) && isCorrectNumberOfDayNotLeapYear(day, month))) &&
                LocalDate.parse(str, DateTimeFormatter.ofPattern("d.M.y")).isBefore(LocalDate.now());
    }


    private static boolean isCorrectNumberOfDayNotLeapYear(int day, int month) {
        return day <= DAYS_OF_NOT_LEAP_YEAR[month - 1];
    }

    private static boolean isCorrectNumberOfDayLeapYear(int day, int month) {
        return day <= DAYS_OF_LEAP_YEAR[month - 1];
    }

    private static boolean isLeapYear(int year) {
        return ((year & 3) == 0) && ((year % 100) != 0 || (year % 400) == 0);
    }

    private static boolean isRightGender(String str) {
        boolean result = false;
        for (Gender value : Gender.values()) {
            if (value.toString().equals(str)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
