package pm.hw1.additional.task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int max = sc.nextInt();
        int previous = sc.nextInt();
        n -= 2;
        while (n-- > 0) {
            int num = sc.nextInt();
            if (num > max) {
                previous = max;
                max = num;
            } else if (num > previous) {
                previous = num;
            }
        }
        System.out.printf("Max num: %d\nNext max num: %d\n", max, previous);
    }
}
