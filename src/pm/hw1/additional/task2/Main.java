package pm.hw1.additional.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[sc.nextInt()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = sc.nextInt();
        }
        int n = sc.nextInt();
        System.out.println(binarySearch(arr, n, 0, arr.length - 1));
    }

    private static int binarySearch(int[] arr, int val, int start, int end) {
        if (start <= end) {
            int mid = (end + start) / 2;
            if (arr[mid] == val) {
                return mid;
            }
            if (arr[mid] > val) {
                return binarySearch(arr, val, start, mid - 1);
            }
            if (arr[mid] < val) {
                return binarySearch(arr, val, mid + 1, end);
            }
        }
        return -1;
    }
}
