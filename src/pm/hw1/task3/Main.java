package pm.hw1.task3;

import java.io.*;

public class Main {

    private static final String DIRECTORY = "/home/user/IdeaProjects/sbercourses/src/pm/hw1/task3/";
    private static final String OUTPUT_FILE = "output.txt";
    private static final String INPUT_FILE = "input.txt";

    public static void main(String[] args) {
        fromInToOut();

        /* Результат работы программы
        HELLO WORLD!
        AZ***12345^&**()_+? / ><":"{{***AZ
        QWERTYUIOPASDFGHJKLZXCVBNM
        QWERTYUIOPASDFGHJKLZXCVBNM
         */
    }

    public static void fromInToOut() {
        try (BufferedReader br = new BufferedReader(new FileReader(new File(DIRECTORY + INPUT_FILE)));
             BufferedWriter bw = new BufferedWriter(new FileWriter(new File(DIRECTORY + OUTPUT_FILE)))) {
            String line;
            while ((line = br.readLine()) != null) {
                bw.write(toUpperCase(line) + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String toUpperCase(String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < str.length()) {
            char c = str.charAt(i);
            sb.append((c > 96 && c < 123) ? (char) (c - 32) : c);
            i++;
        }
        return sb.toString();
    }
}
