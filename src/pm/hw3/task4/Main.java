package pm.hw3.task4;

import pm.hw3.task4.clases.C4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Class<?>> list = getAllInterfaces(C4.class);
        for (Class<?> cls : list) {
            System.out.println(cls.getName());
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> list = new ArrayList<>();
        while (cls != Object.class) {
            // Создаем список интерфейсов, реализуемые классом
            List<Class<?>> interfaces = Arrays.asList(cls.getInterfaces());
            // Проходимся по каждому интерфейсу класса и рекурсивно добавляем все интерфесы, которые расширяет
            // рассматриваемый интерфейс
            for (Class<?> inter : interfaces) {
                List<Class<?>> tmp = new ArrayList<>();
                list.addAll(getParentInterfaces(inter, tmp));
            }
            list.addAll(interfaces);
            cls = cls.getSuperclass();
        }
        return list;
    }

    private static List<Class<?>> getParentInterfaces(Class<?> inter, List<Class<?>> recList) {
        if (inter == null) {
            return recList;
        }
        Class<?>[] interfaces = inter.getInterfaces();
        for (Class<?> one : interfaces) {
            recList.add(one);
            getParentInterfaces(one, recList);
        }
        return recList;
    }
}
