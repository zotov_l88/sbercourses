package pm.hw3.additional.task2;

import java.util.Stack;

public class Main {

    private static final String BRACKETS = ")]}";

    public static void main(String[] args) {
        String[] array = {
                "{()[]()}",                 // true;
                "{)(}",                     // false
                "[}",                       // false
                "[{(){}}][()]{}",           // true
                "",                         // true
                "{1(1, 2, 3)>[<]/(xx)+}",   // true
                "((){}[]}",                 // false
        };

        for (String str : array) {
            System.out.println(isCorrectSequence(str));
        }
    }

    private static boolean isCorrectSequence(String sequence) {
        if (sequence.isBlank()) {
            return true;
        }
        Stack<Character> stack = new Stack<>();
        for (char c : sequence.toCharArray()) {
            if (c == '(') stack.push(')');
            if (c == '[') stack.push(']');
            if (c == '{') stack.push('}');
            if (BRACKETS.contains(Character.toString(c)) && (stack.isEmpty() || stack.pop() != c)) {
                return false;
            }
        }
        return stack.isEmpty();
    }
}
