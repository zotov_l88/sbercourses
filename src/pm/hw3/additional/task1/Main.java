package pm.hw3.additional.task1;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        String[] array = {
                "",                         // true;
                "without brackets",         // true
                "(with)",                   // true
                "(it's)((wrong)",           // false
                "(()()())",                 // true
                ")(",                       // false
                "(()",                      // false
                "((()))",                   // true
                "((((()))((()))((()))))",   // true
                "((((()))((()))((())))"     // false
        };
        for (String str : array) {
            System.out.println(isCorrectSequence(str));
        }
    }

    private static boolean isCorrectSequence(String sequence) {
        if (sequence.isBlank()) {
            return true;
        }
        Stack<Character> stack = new Stack<>();
        for (char c : sequence.toCharArray()) {
            if (c == '(') stack.push(')');
            if (c == ')' && (stack.isEmpty() || stack.pop() != c)) {
                return false;
            }
        }
        return stack.isEmpty();
    }
}
