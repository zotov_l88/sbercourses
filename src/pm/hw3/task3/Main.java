package pm.hw3.task3;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {

        Class<APrinter> cls = APrinter.class;
        try {
            Constructor<APrinter> constructor = cls.getDeclaredConstructor();
            Method method = cls.getDeclaredMethod("print", int.class);
            method.invoke(constructor.newInstance(), 11);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Method is not found");
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Error when calling a method or constructor");
        } catch (InstantiationException e) {
            throw new RuntimeException("Class object could not be created");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Executing method does not have access to the definition of the specified class, field, method or constructor");
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Wrong value");
        }
    }
}
