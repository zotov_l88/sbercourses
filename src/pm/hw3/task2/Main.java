package pm.hw3.task2;

import pm.hw3.task1.IsLike;

public class Main {
    public static void main(String[] args) {
        printValueIsLikeAnnotation(Test3.class);
        printValueIsLikeAnnotation(Test2.class);
        printValueIsLikeAnnotation(Test1.class);
    }

    public static void printValueIsLikeAnnotation(Class<?> cls) {
        System.out.printf("%s\n", cls.isAnnotationPresent(IsLike.class) ?
                cls.getAnnotation(IsLike.class).isChecked() : "");
    }
}
