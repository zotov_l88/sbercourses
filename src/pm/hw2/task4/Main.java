package pm.hw2.task4;

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        for (int i = 0; i < 20; i++) {
            Documents.documentsList.add(new Document(i + 1, "Doc " + (i + 1), new Random().nextInt(50)));
        }

        System.out.println("Documents into list:");
        System.out.println(Documents.documentsList);
        System.out.println("Documents into map:");
        System.out.println(Documents.organizeDocuments(Documents.documentsList));
    }
}
