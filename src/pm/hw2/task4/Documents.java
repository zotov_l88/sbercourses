package pm.hw2.task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Documents {

    public static final ArrayList<Document> documentsList = new ArrayList<>();

    private Documents() {
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> documentsMap = new HashMap<>();
        for (Document document : documents) {
            documentsMap.put(document.getId(), document);
        }
        return documentsMap;
    }
}
