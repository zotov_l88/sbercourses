package pm.hw2.task1;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("one", "five", "two", "one", "four", "one", "three", "four", "five"));
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 2, 1, 2, 2, 3, 4, 2, 4, 5, 2, 7, 8, 4, 3, 2, 9, 4));
        System.out.println(uniqueOnly(list1));
        System.out.println(uniqueOnly(list2));
    }

    public static <T> Set<T> uniqueOnly(List<T> list) {
        return new HashSet<>(list);
    }
}
