package pm.hw2.task2;

import java.util.ArrayList;
import java.util.Collections;


public class Anagram {

    public static void main(String[] args) {

        System.out.println(isAnagram("Hello World!", " rlod!WleolH")); // true
        System.out.println(isAnagram("Hello World!", " rlod!WltolH")); // false
        System.out.println(isAnagram("Hello W!", "Hello !W")); // true
        System.out.println(isAnagram(null, "null")); // false
        System.out.println(isAnagram("1", "12")); // false
        System.out.println(isAnagram("1234567890", "0987654321")); // true

    }

    private static boolean isAnagram(String s1, String s2) {
        if ((s1 == null || s2 == null) || s1.length() != s2.length()) {
            return false;
        }
        ArrayList<Character> list1 =  getList(s1);
        ArrayList<Character> list2 =  getList(s2);
        Collections.sort(list1);
        Collections.sort(list2);
        return list1.equals(list2);
    }

    private static ArrayList<Character> getList(String s) {
        ArrayList<Character> list = new ArrayList<>();
        int i = s.length() - 1;
        while (i >= 0) {
            list.add(s.charAt(i--));
        }
        return list;
    }
}
