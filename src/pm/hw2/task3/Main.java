package pm.hw2.task3;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Main {

    public static final String[] WORDS = {
            "Lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipisci", "elit", "sed",
            "eiusmod", "tempor", "incidunt", "ut", "labore", "et", "dolore", "magna", "aliqua", "Ut",
            "enim", "ad", "minim", "veniam", "quis", "nostrum", "exercitationem", "ullam", "corporis", "suscipit",
    };

    public static Set<Integer> set1;
    public static Set<Integer> set2;
    public static Set<String> set3;
    public static Set<String> set4;

    public static final int N = 10;

    public static void main(String[] args) {

        fillSetInteger();
        fillSetString();
        showSets(set1, set2);
        System.out.println("After intersection: " + PowerfulSet.intersection(set1, set2));
        showSets(set3, set4);
        System.out.println("After intersection: " + PowerfulSet.intersection(set3, set4));
        System.out.println("----------------------------------------------------------------------");

        fillSetInteger();
        fillSetString();
        showSets(set1, set2);
        System.out.println("After union: " + PowerfulSet.union(set1, set2));
        showSets(set3, set4);
        System.out.println("After union: " + PowerfulSet.union(set3, set4));
        System.out.println("----------------------------------------------------------------------");

        fillSetInteger();
        fillSetString();
        showSets(set1, set2);
        System.out.println("After relativeComplement: " + PowerfulSet.relativeComplement(set1, set2));
        showSets(set3, set4);
        System.out.println("After relativeComplement: " + PowerfulSet.relativeComplement(set3, set4));
        System.out.println("----------------------------------------------------------------------");
    }

    public static <T> void showSets(Set<T> set1, Set<T> set2) {
        System.out.println("Set 1: " + set1);
        System.out.println("Set 2: " + set2);
    }

    private static void fillSetInteger() {
        set1 = new HashSet<>();
        set2 = new HashSet<>();
        Random random = new Random();
        for (int i = 0; i < N; i++) {
            set1.add(random.nextInt(20));
            set2.add(random.nextInt(20));
        }
    }

    private static void fillSetString() {
        set3 = new HashSet<>();
        set4 = new HashSet<>();
        Random random = new Random();
        for (int i = 0; i < N; i++) {
            set3.add(WORDS[random.nextInt(WORDS.length)]);
            set4.add(WORDS[random.nextInt(WORDS.length)]);
        }
    }
}
