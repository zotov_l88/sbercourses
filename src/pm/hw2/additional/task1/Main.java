package pm.hw2.additional.task1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {

        String[] words = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is", "day"};
        int k = 4;
        System.out.println(Arrays.toString(mostPopularityWords(words, k)));
    }

    private static String[] mostPopularityWords(String[] words, int top) {
        HashMap<String, Integer> map = getWordCount(words);
        TreeSet<Pair> treeSet = getPairs(map);
        return getArray(treeSet, top);
    }

    private static HashMap<String, Integer> getWordCount(String[] words) {
        HashMap<String, Integer> map = new HashMap<>();
        for (String word : words) {
            if (map.containsKey(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }
        return map;
    }

    private static TreeSet<Pair> getPairs(HashMap<String, Integer> map) {
        TreeSet<Pair> treeSet = new TreeSet<>();
        for (String word : map.keySet()) {
            treeSet.add(new Pair(map.get(word), word));
        }
        return treeSet;
    }

    private static String[] getArray(TreeSet<Pair> set, int top) {
        String[] array = new String[top];
        int i = 0;
        for (Pair pair : set) {
            if (i > top - 1) {
                break;
            }
            array[i++] = pair.word;
        }
        return array;
    }

    private static class Pair implements Comparable<Pair> {
        private final Integer count;
        private final String word;

        public Pair(int count, String word) {
            this.count = count;
            this.word = word;
        }

        @Override
        public int compareTo(Pair o) {
            int result = o.count.compareTo(this.count);
            result = (result == 0) ? o.word.compareTo(this.word) : result;
            return result;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "count=" + count +
                    ", word='" + word + '\'' +
                    '}';
        }
    }
}
