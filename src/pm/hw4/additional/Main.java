package pm.hw4.additional;

import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        String str1 = "cat";
        String str2 = "ct";
        System.out.println(isCanTEquals(str1, str2));
    }

    private static boolean isCanTEquals(String str1, String str2) {
        return  isCanAddOneSym(str1, str2) || isCanDelOneSym(str1, str2) || isCanToChangeSym(str1, str2);
    }

    private static boolean isCanAddOneSym(String str1, String str2) {
        boolean result = false;
        if (str1.length() - str2.length() == 1) {
            int countOfMistakes = 1;
            int i = 0;
            int j = 0;
            while (countOfMistakes > -1 && j < str2.length()) {
                if (str1.charAt(i) == str2.charAt(j)) {
                    j++;
                } else {
                    countOfMistakes--;
                }
                i++;
            }
            result = countOfMistakes > -1;
        }
        return result;
    }

    private static boolean isCanDelOneSym(String str1, String str2) {
        boolean result = false;
        if (str1.length() - str2.length() == -1) {
            int countOfMistakes = 1;
            int i = 0;
            int j = 0;
            while (countOfMistakes > -1 && i < str1.length()) {
                if (str1.charAt(i) == str2.charAt(j)) {
                    i++;
                } else {
                    countOfMistakes--;
                }
                j++;
            }
            result = countOfMistakes > -1;
        }
        return result;
    }

    private static boolean isCanToChangeSym(String str1, String str2) {
        boolean result = false;
        if (str1.length() == str2.length()) {
            result = true;
            Stack<Character> stack = new Stack<>();
            int countOfMistakes = 1;
            for (int i = 0; i < str1.length(); i++) {
                stack.push(str1.charAt(i));
                if (str2.charAt(i) != stack.peek()) {
                    countOfMistakes--;
                }
                if (countOfMistakes < 0) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }
}
