package pm.hw4.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }
        System.out.println(sumEvenNums(list));
    }

    private static int sumEvenNums(List<Integer> list) {
        return list.stream()
                .filter((n) -> n % 2 == 0)
                .mapToInt((n) -> n)
                .sum();
    }
}
