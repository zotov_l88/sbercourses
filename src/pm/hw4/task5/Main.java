package pm.hw4.task5;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        foo(list);
    }

    private static void foo(List<String> list) {
        System.out.println(list.stream()
                .map((String::toUpperCase))
                .collect(Collectors.joining(", ")));
    }
}
