package pm.hw4.task4;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(9d, 0.99, 1.23, 4.42, 4.02, 11.11, 7.1);
        System.out.println(sortList(list));
    }

    private static List<Double> sortList(List<Double> list) {
        return list.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
    }
}
