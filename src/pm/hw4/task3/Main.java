package pm.hw4.task3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        System.out.println(numsNonEmptyString(list));
    }

    private static long numsNonEmptyString(List<String> list) {
        return list.stream()
                .filter((String::isEmpty))
                .count();
    }
}
