package pm.hw4.task6;

import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Set<Integer> one = Set.of(1, 2, 3);
        Set<Integer> two = Set.of(2, 3, 4);
        Set<Integer> three = Set.of(4, 5, 6);
        Set<Set<Integer>> set = Set.of(one, two, three);
        System.out.println(toSet(set));
    }

    private static Set<Integer> toSet(Set<Set<Integer>> set) {
        return set.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
    }
}
