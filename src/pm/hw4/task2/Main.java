package pm.hw4.task2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        System.out.println(multAllElements(list));
    }

    private static int multAllElements(List<Integer> list) {
        return list.stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);
    }
}
